webpackJsonp([4],{

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MeusDadosPageModule", function() { return MeusDadosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__meus_dados__ = __webpack_require__(332);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MeusDadosPageModule = /** @class */ (function () {
    function MeusDadosPageModule() {
    }
    MeusDadosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__meus_dados__["a" /* MeusDadosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__meus_dados__["a" /* MeusDadosPage */]),
            ],
        })
    ], MeusDadosPageModule);
    return MeusDadosPageModule;
}());

//# sourceMappingURL=meus-dados.module.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeusDadosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__ = __webpack_require__(12);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






/**
 * Generated class for the MeusDadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MeusDadosPage = /** @class */ (function () {
    function MeusDadosPage(alertCtrl, firebase, notificacao, camera, navCtrl, navParams, usuario) {
        this.alertCtrl = alertCtrl;
        this.firebase = firebase;
        this.notificacao = notificacao;
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.usuario = usuario;
        this.acesso = {
            email: '',
            senha: '',
            novaSenha: '',
            confirmacaoNovaSenha: '',
        };
        this.foto = {
            capa: '',
            perfil: '',
        };
        this.dados = __assign({}, this.usuario.get());
        this.acesso.email = this.usuario.get().email;
    }
    MeusDadosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MeusDadosPage');
    };
    MeusDadosPage.prototype.alterarFotoCapa = function (tipo) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 800,
            targetHeight: 800,
            correctOrientation: true
        };
        if (tipo == 'camera') {
            options.sourceType = this.camera.PictureSourceType.CAMERA;
        }
        else {
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        }
        this.camera.getPicture(options).then(function (imageData) {
            _this.foto.capa = imageData;
            _this.dados.fotoCapa = 'data:image/jpeg;base64,' + imageData;
        }, function (err) { });
    };
    MeusDadosPage.prototype.alterarFotoPerfil = function (tipo) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 800,
            targetHeight: 800,
            correctOrientation: true
        };
        if (tipo == 'camera') {
            options.sourceType = this.camera.PictureSourceType.CAMERA;
        }
        else {
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        }
        this.camera.getPicture(options).then(function (imageData) {
            _this.foto.perfil = imageData;
            _this.dados.fotoPerfil = 'data:image/jpeg;base64,' + imageData;
        }, function (err) { });
    };
    MeusDadosPage.prototype.cancelar = function () {
        this.dados = __assign({}, this.usuario.get());
        this.acesso.email = this.usuario.get().email;
        this.acesso.senha = '';
        this.acesso.novaSenha = '';
        this.acesso.confirmacaoNovaSenha = '';
    };
    MeusDadosPage.prototype.salvar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var storageRef, storageRef_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //Verificação de confirmação de senha
                        if (this.acesso.novaSenha.length > 0) {
                            if (this.acesso.novaSenha == this.acesso.confirmacaoNovaSenha) {
                                if (this.acesso.senha == '') {
                                    this.notificacao.adicionarMensagem('Para alterar o email ou senha é necessário inserir a senha atual', this.notificacao.BOTTOM).mostrarNotificacoes();
                                    return [2 /*return*/];
                                }
                            }
                            else {
                                this.notificacao.adicionarMensagemErro('A confirmação de nova senha está inválida!', this.notificacao.BOTTOM).mostrarNotificacoes();
                                return [2 /*return*/];
                            }
                        }
                        if (this.acesso.email != this.usuario.get().email) {
                            if (this.acesso.senha == '') {
                                this.notificacao.adicionarMensagem('Para alterar o email ou senha é necessário inserir a senha atual!', this.notificacao.BOTTOM).mostrarNotificacoes();
                                return [2 /*return*/];
                            }
                        }
                        this.notificacao.mostrarMensagemAguarde('Aguarde a confirmação ...', this.notificacao.BOTTOM);
                        if (!(this.acesso.novaSenha.length > 0 || this.usuario.get().email != this.acesso.email)) return [3 /*break*/, 2];
                        //Confirmação de senha
                        return [4 /*yield*/, this.firebase.auth().signInWithEmailAndPassword(this.usuario.get().email, this.acesso.senha).then(function () { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                var user;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            user = this.firebase.auth().currentUser;
                                            if (!(this.usuario.get().email != this.acesso.email)) return [3 /*break*/, 2];
                                            //Alteração de email
                                            return [4 /*yield*/, user.updateEmail(this.dados.email).then(function () {
                                                    _this.notificacao.adicionarMensagemSucesso('Email alterado com sucesso!', _this.notificacao.BOTTOM);
                                                }).catch(function (erro) {
                                                    _this.notificacao.adicionarMensagemErro('O email não pode ser alterado!', _this.notificacao.BOTTOM);
                                                })];
                                        case 1:
                                            //Alteração de email
                                            _a.sent();
                                            _a.label = 2;
                                        case 2:
                                            if (!(this.acesso.novaSenha.length > 0)) return [3 /*break*/, 4];
                                            //Alteração de senha
                                            return [4 /*yield*/, user.updatePassword(this.acesso.novaSenha).then(function () {
                                                    _this.notificacao.adicionarMensagemSucesso('Senha alterada com sucesso!', _this.notificacao.BOTTOM);
                                                }).catch(function (erro) {
                                                    _this.notificacao.adicionarMensagemErro('A senha não pode ser alterada!', _this.notificacao.BOTTOM);
                                                })];
                                        case 3:
                                            //Alteração de senha
                                            _a.sent();
                                            _a.label = 4;
                                        case 4: return [2 /*return*/];
                                    }
                                });
                            }); }).catch(function (erro) {
                                _this.notificacao.adicionarMensagemErro('A senha atual está incorreta! Senha não alterada!', _this.notificacao.BOTTOM);
                            })];
                    case 1:
                        //Confirmação de senha
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(this.foto.capa.length > 0)) return [3 /*break*/, 4];
                        storageRef = this.firebase.storage().ref('capa/' + this.usuario.get().uid + '.jpg');
                        return [4 /*yield*/, storageRef.putString(this.foto.capa, 'base64', { contentType: 'image/jpeg' })];
                    case 3:
                        _a.sent();
                        storageRef.getDownloadURL().then(function (url) {
                            _this.dados.fotoCapa = url;
                        }).catch(function (erro) {
                            _this.notificacao.adicionarMensagemErro('Não foi possível alterar imagem da capa!', _this.notificacao.BOTTOM);
                            _this.dados.fotoCapa = _this.usuario.get().fotoCapa;
                            _this.foto.capa;
                        });
                        _a.label = 4;
                    case 4:
                        if (!(this.foto.perfil.length > 0)) return [3 /*break*/, 6];
                        storageRef_1 = this.firebase.storage().ref('perfil/' + this.usuario.get().uid + '.jpg');
                        return [4 /*yield*/, storageRef_1.putString(this.foto.perfil, 'base64', { contentType: 'image/jpeg' }).then(function () { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, storageRef_1.getDownloadURL().then(function (url) {
                                                _this.dados.fotoPerfil = url;
                                            }).catch(function (erro) {
                                                _this.notificacao.adicionarMensagemErro('Não foi possível alterar a foto de perfil!', _this.notificacao.BOTTOM);
                                                _this.dados.fotPerfil = _this.usuario.get().fotoPerfil;
                                                _this.foto.perfil;
                                            })];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); }).catch(function (erro) {
                                _this.notificacao.adicionarMensagemErro('Não foi possível alterar a foto de perfil!', _this.notificacao.BOTTOM);
                                _this.dados.fotoPerfil = _this.usuario.get().fotoPerfil;
                                _this.foto.perfil;
                            })];
                    case 5:
                        _a.sent();
                        _a.label = 6;
                    case 6: 
                    //Salva os dados
                    return [4 /*yield*/, this.firebase.db().collection("usuarios").where('uid', '==', this.usuario.get().uid).get().then(function (result) {
                            result.docs.forEach(function (doc) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, this.firebase.db().collection("usuarios").doc(doc.id).update(__assign({}, this.dados)).then(function (result) {
                                                _this.notificacao.adicionarMensagemSucesso('Dados alterados com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                            }).catch(function (erro) {
                                                _this.notificacao.adicionarMensagemErro('Não foi possível alterar os dados!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                            })];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); });
                        }).catch(function (erro) {
                            _this.notificacao.adicionarMensagemErro('Não foi possível alterar os dados!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        })];
                    case 7:
                        //Salva os dados
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MeusDadosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-meus-dados',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/meus-dados/meus-dados.html"*/'<!--\n\n  Generated template for the MeusDadosPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar color="dark">\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Meus Dados</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div>\n\n    <div class="perfil">\n\n      <div class="capa">\n\n        <img src="{{ dados.fotoCapa }}">\n\n        <div class="foto">\n\n          <img src="{{ dados.fotoPerfil }}">\n\n        </div>\n\n      </div>\n\n      <ion-fab class="fab-image">\n\n        <button ion-fab color="light" mini>\n\n          <ion-icon name="create"></ion-icon>\n\n        </button>\n\n        <ion-fab-list side="bottom">\n\n          <button ion-fab mini (click)="alterarFotoCapa(\'arquivo\')">\n\n            <ion-icon name="image"></ion-icon>\n\n            <ion-label>Imagem da Capa</ion-label>\n\n          </button>\n\n          <button ion-fab mini (click)="alterarFotoCapa(\'camera\')">\n\n            <ion-icon name="camera"></ion-icon>\n\n            <ion-label>Imagem da Capa</ion-label>\n\n          </button>\n\n          <button ion-fab mini (click)="alterarFotoPerfil(\'arquivo\')">\n\n            <ion-icon name="image"></ion-icon>\n\n            <ion-label>Foto do Perfil</ion-label>\n\n          </button>\n\n          <button ion-fab mini (click)="alterarFotoPerfil(\'camera\')">\n\n            <ion-icon name="camera"></ion-icon>\n\n            <ion-label>Foto do Perfil</ion-label>\n\n          </button>\n\n        </ion-fab-list>\n\n      </ion-fab>\n\n    </div>\n\n    <div class="form">\n\n      <ion-item>\n\n        <ion-label floating>Nome</ion-label>\n\n        <ion-input type="text" [(ngModel)]="dados.nome"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Bio</ion-label>\n\n        <ion-input type="text" [(ngModel)]="dados.bio"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Telefone</ion-label>\n\n        <ion-input type="text" [(ngModel)]="dados.telefone"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Email</ion-label>\n\n        <ion-input type="text" [(ngModel)]="acesso.email"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n          <ion-label floating>Senha Atual</ion-label>\n\n          <ion-input type="password" [(ngModel)]="acesso.senha"></ion-input>\n\n        </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Nova Senha</ion-label>\n\n        <ion-input type="password" [(ngModel)]="acesso.novaSenha"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Confirmação da Nova Senha</ion-label>\n\n        <ion-input type="password" [(ngModel)]="acesso.confirmacaoNovaSenha"></ion-input>\n\n      </ion-item>\n\n    </div>\n\n    <div class="botoes">\n\n        <button ion-button secondary round full (click)="salvar()">Salvar</button>\n\n        <button ion-button color="danger" round full (click)="cancelar()">Cancelar</button>\n\n      </div>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/meus-dados/meus-dados.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */]])
    ], MeusDadosPage);
    return MeusDadosPage;
}());

//# sourceMappingURL=meus-dados.js.map

/***/ })

});
//# sourceMappingURL=4.js.map