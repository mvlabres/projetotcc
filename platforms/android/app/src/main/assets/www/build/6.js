webpackJsonp([6],{

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriarPostPageModule", function() { return CriarPostPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__criar_post__ = __webpack_require__(330);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CriarPostPageModule = /** @class */ (function () {
    function CriarPostPageModule() {
    }
    CriarPostPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__criar_post__["a" /* CriarPostPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__criar_post__["a" /* CriarPostPage */]),
            ],
        })
    ], CriarPostPageModule);
    return CriarPostPageModule;
}());

//# sourceMappingURL=criar-post.module.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CriarPostPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_usuario_usuario__ = __webpack_require__(18);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the CriarPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CriarPostPage = /** @class */ (function () {
    function CriarPostPage(view, navCtrl, navParams, firebase, camera, http, notificacao, usuario) {
        this.view = view;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firebase = firebase;
        this.camera = camera;
        this.http = http;
        this.notificacao = notificacao;
        this.usuario = usuario;
        this.dados = {
            foto: 'assets/imgs/default-banner.jpg',
            titulo: '',
            mensagem: '',
            data: new Date().toJSON().split('T')[0],
        };
        this.foto = '';
    }
    CriarPostPage.prototype.alterarImagem = function (tipo) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 800,
            targetHeight: 800,
            correctOrientation: true
        };
        if (tipo == 'camera') {
            options.sourceType = this.camera.PictureSourceType.CAMERA;
        }
        else {
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        }
        this.camera.getPicture(options).then(function (imageData) {
            _this.foto = imageData;
            _this.dados.foto = 'data:image/jpeg;base64,' + imageData;
        }, function (err) { });
    };
    CriarPostPage.prototype.salvar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var storageRef;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        storageRef = this.firebase.storage().ref('eventos/default-banner.jpg');
                        return [4 /*yield*/, storageRef.getDownloadURL().then(function (url) {
                                _this.dados.foto = url;
                            })];
                    case 1:
                        _a.sent();
                        //Salva dados do post
                        return [4 /*yield*/, this.firebase.db().collection('post').add(__assign({ idUsuario: this.usuario.get().uid }, this.dados, { dataPublicacao: new Date().toJSON().split('T')[0], dataAlteracao: new Date().toJSON().split('T')[0] })).then(function (doc) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                var storageRef_1;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!(this.foto.length > 0)) return [3 /*break*/, 2];
                                            storageRef_1 = this.firebase.storage().ref('post/' + doc.id + '.jpg');
                                            return [4 /*yield*/, storageRef_1.putString(this.foto, 'base64', { contentType: 'image/jpeg' }).then(function () { return __awaiter(_this, void 0, void 0, function () {
                                                    var _this = this;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0: return [4 /*yield*/, storageRef_1.getDownloadURL().then(function (url) { return __awaiter(_this, void 0, void 0, function () {
                                                                    var _this = this;
                                                                    return __generator(this, function (_a) {
                                                                        switch (_a.label) {
                                                                            case 0:
                                                                                this.dados.foto = url;
                                                                                return [4 /*yield*/, this.firebase.db().collection('post').doc(doc.id).update({
                                                                                        foto: this.dados.foto
                                                                                    }).then(function () { }).catch(function (error) {
                                                                                        _this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do post!', _this.notificacao.BOTTOM);
                                                                                        _this.dados.foto = 'assets/imgs/default-banner.jpg';
                                                                                        _this.foto = '';
                                                                                    })];
                                                                            case 1:
                                                                                _a.sent();
                                                                                return [2 /*return*/];
                                                                        }
                                                                    });
                                                                }); }).catch(function (erro) {
                                                                    _this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do post!', _this.notificacao.BOTTOM);
                                                                    _this.dados.foto = 'assets/imgs/default-banner.jpg';
                                                                    _this.foto = '';
                                                                })];
                                                            case 1:
                                                                _a.sent();
                                                                return [2 /*return*/];
                                                        }
                                                    });
                                                }); }).catch(function (erro) {
                                                    _this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do post!', _this.notificacao.BOTTOM);
                                                    _this.dados.foto = 'assets/imgs/default-banner.jpg';
                                                    _this.foto = '';
                                                })];
                                        case 1:
                                            _a.sent();
                                            _a.label = 2;
                                        case 2:
                                            this.notificacao.adicionarMensagemSucesso('Post criado com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
                                            this.fechar();
                                            return [2 /*return*/];
                                    }
                                });
                            }); }).catch(function (error) {
                                _this.notificacao.adicionarMensagemErro('Erro ao criar post!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                            })];
                    case 2:
                        //Salva dados do post
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CriarPostPage.prototype.fechar = function () {
        this.view.dismiss();
    };
    CriarPostPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CriarPostPage');
    };
    CriarPostPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-criar-post',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/criar-post/criar-post.html"*/'<!--\n  Generated template for the CriarPostPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button class="icon-large">\n        <ion-icon name="create"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Criar Post</ion-title>\n    <ion-buttons right>\n      <button ion-button class="icon-large" (click)="fechar()">\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div class="image">\n    <ion-fab top right>\n      <button ion-fab color="light" mini>\n        <ion-icon name="create"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab mini (click)="alterarImagem(\'arquivo\')">\n          <ion-icon name="image"></ion-icon>\n        </button>\n        <button ion-fab mini (click)="alterarImagem(\'camera\')">\n          <ion-icon name="camera"></ion-icon>\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n    <img [src]="dados.foto">\n  </div>\n  <div class="form">\n    <ion-item>\n        <ion-label floating>Qual o título da sua postagem</ion-label>\n        <input type="text" rows="4" [(ngModel)]="dados.titulo"></input>\n      <ion-label floating>Escreva aqui o que você está pensando</ion-label>\n      <ion-textarea type="text" rows="4" [(ngModel)]="dados.mensagem"></ion-textarea>\n    </ion-item>\n  </div>\n  <div class="botoes">\n    <button ion-button block (click)="salvar()">Salvar</button>\n    <button ion-button block clear  (click)="cancelar()">Cancelar</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/criar-post/criar-post.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_usuario_usuario__["a" /* UsuarioProvider */]])
    ], CriarPostPage);
    return CriarPostPage;
}());

//# sourceMappingURL=criar-post.js.map

/***/ })

});
//# sourceMappingURL=6.js.map