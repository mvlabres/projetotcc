webpackJsonp([8],{

/***/ 118:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 118;

/***/ }),

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirebaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_firebase_firestore__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var FirebaseProvider = /** @class */ (function () {
    function FirebaseProvider() {
        var config = {
            apiKey: "AIzaSyAkumw4cF2V8HI6GFBRm9v8aHI-qU6qcA4",
            authDomain: "app-crj.firebaseapp.com",
            databaseURL: "https://app-crj.firebaseio.com",
            projectId: "app-crj",
            storageBucket: "app-crj.appspot.com",
            messagingSenderId: "1059914658044"
        };
        __WEBPACK_IMPORTED_MODULE_1_firebase__["initializeApp"](config);
    }
    FirebaseProvider.prototype.db = function () {
        return __WEBPACK_IMPORTED_MODULE_1_firebase__["firestore"]();
    };
    FirebaseProvider.prototype.storage = function () {
        return __WEBPACK_IMPORTED_MODULE_1_firebase__["storage"]();
    };
    FirebaseProvider.prototype.auth = function () {
        return __WEBPACK_IMPORTED_MODULE_1_firebase__["auth"]();
    };
    FirebaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], FirebaseProvider);
    return FirebaseProvider;
}());

//# sourceMappingURL=firebase.js.map

/***/ }),

/***/ 15:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacaoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the NotificacaoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var NotificacaoProvider = /** @class */ (function () {
    function NotificacaoProvider(toast, loadingCtrl) {
        this.toast = toast;
        this.loadingCtrl = loadingCtrl;
        this.TOP = 'top';
        this.MIDDLE = 'middle';
        this.BOTTOM = 'bottom';
        this.notificacoes = [];
        //
    }
    //Adiciona as notificações na fila (usada dentro dos metodos adicionarMensagem...)
    NotificacaoProvider.prototype.adicionarNotificacao = function (notificacao) {
        var _this = this;
        this.notificacoes.push(notificacao);
        notificacao.onDidDismiss(function () {
            _this.notificacoes.shift();
            if (_this.notificacoes.length > 0) {
                var next = _this.notificacoes[0];
                next.present();
            }
        });
        return this;
    };
    //Inicia execução da fila de mensagens
    NotificacaoProvider.prototype.mostrarNotificacoes = function () {
        if (this.notificacoes.length > 0) {
            var next = this.notificacoes[0];
            next.present();
        }
    };
    //Mostra mensagem de espera ("Aguarde Confirmação ...")
    NotificacaoProvider.prototype.mostrarMensagemAguarde = function (mensagem, posicao) {
        this.toast.create({
            message: mensagem,
            duration: 2000,
            position: posicao,
        }).present();
    };
    //Adiciona mensagem padrão na fila de mensagens
    NotificacaoProvider.prototype.adicionarMensagem = function (mensagem, posicao) {
        this.adicionarNotificacao(this.toast.create({
            message: mensagem,
            duration: 2000,
            position: posicao,
        }));
        return this;
    };
    //Adiciona mensagem de sucesso na fila de mensagens
    NotificacaoProvider.prototype.adicionarMensagemSucesso = function (mensagem, posicao) {
        this.adicionarNotificacao(this.toast.create({
            message: mensagem,
            duration: 2000,
            position: posicao,
            cssClass: 'toast-sucesso',
        }));
        return this;
    };
    //Adiciona mensagem de erro na fila de mensagens
    NotificacaoProvider.prototype.adicionarMensagemErro = function (mensagem, posicao) {
        this.adicionarNotificacao(this.toast.create({
            message: mensagem,
            duration: 2000,
            position: posicao,
            cssClass: 'toast-erro',
        }));
        return this;
    };
    //Adiciona mensagem de aviso na fila de mensagens
    NotificacaoProvider.prototype.adicionarMensagemAviso = function (mensagem, posicao) {
        this.adicionarNotificacao(this.toast.create({
            message: mensagem,
            duration: 2000,
            position: posicao,
            cssClass: 'toast-aviso',
        }));
        return this;
    };
    NotificacaoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], NotificacaoProvider);
    return NotificacaoProvider;
}());

//# sourceMappingURL=notificacao.js.map

/***/ }),

/***/ 159:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/criar-conta/criar-conta.module": [
		164
	],
	"../pages/criar-evento/criar-evento.module": [
		321,
		7
	],
	"../pages/criar-post/criar-post.module": [
		322,
		6
	],
	"../pages/criar/criar.module": [
		323,
		5
	],
	"../pages/editar-evento/editar-evento.module": [
		165
	],
	"../pages/evento/evento.module": [
		177
	],
	"../pages/eventos/eventos.module": [
		172
	],
	"../pages/home/home.module": [
		166
	],
	"../pages/igrejas/igrejas.module": [
		168
	],
	"../pages/login/login.module": [
		167
	],
	"../pages/mapa/mapa.module": [
		169
	],
	"../pages/meus-dados/meus-dados.module": [
		324,
		4
	],
	"../pages/meus-eventos/meus-eventos.module": [
		173
	],
	"../pages/minha-agenda/minha-agenda.module": [
		174
	],
	"../pages/perfil/perfil.module": [
		176
	],
	"../pages/redefinir-senha/redefinir-senha.module": [
		175
	],
	"../pages/relatorios/relatorios.module": [
		327,
		3
	],
	"../pages/sobre/sobre.module": [
		325,
		2
	],
	"../pages/solicitacoes/solicitacoes.module": [
		326,
		1
	],
	"../pages/usuarios/usuarios.module": [
		328,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 159;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriarContaPageModule", function() { return CriarContaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__criar_conta__ = __webpack_require__(290);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CriarContaPageModule = /** @class */ (function () {
    function CriarContaPageModule() {
    }
    CriarContaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__criar_conta__["a" /* CriarContaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__criar_conta__["a" /* CriarContaPage */]),
            ],
        })
    ], CriarContaPageModule);
    return CriarContaPageModule;
}());

//# sourceMappingURL=criar-conta.module.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarEventoPageModule", function() { return EditarEventoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__editar_evento__ = __webpack_require__(291);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EditarEventoPageModule = /** @class */ (function () {
    function EditarEventoPageModule() {
    }
    EditarEventoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__editar_evento__["a" /* EditarEventoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__editar_evento__["a" /* EditarEventoPage */]),
            ],
        })
    ], EditarEventoPageModule);
    return EditarEventoPageModule;
}());

//# sourceMappingURL=editar-evento.module.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(292);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(293);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IgrejasPageModule", function() { return IgrejasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__igrejas__ = __webpack_require__(294);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var IgrejasPageModule = /** @class */ (function () {
    function IgrejasPageModule() {
    }
    IgrejasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__igrejas__["a" /* IgrejasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__igrejas__["a" /* IgrejasPage */]),
            ],
        })
    ], IgrejasPageModule);
    return IgrejasPageModule;
}());

//# sourceMappingURL=igrejas.module.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapaPageModule", function() { return MapaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mapa__ = __webpack_require__(295);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MapaPageModule = /** @class */ (function () {
    function MapaPageModule() {
    }
    MapaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__mapa__["a" /* MapaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mapa__["a" /* MapaPage */]),
            ],
        })
    ], MapaPageModule);
    return MapaPageModule;
}());

//# sourceMappingURL=mapa.module.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventosPageModule", function() { return EventosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eventos__ = __webpack_require__(296);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EventosPageModule = /** @class */ (function () {
    function EventosPageModule() {
    }
    EventosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__eventos__["a" /* EventosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__eventos__["a" /* EventosPage */]),
            ],
        })
    ], EventosPageModule);
    return EventosPageModule;
}());

//# sourceMappingURL=eventos.module.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MeusEventosPageModule", function() { return MeusEventosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__meus_eventos__ = __webpack_require__(297);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MeusEventosPageModule = /** @class */ (function () {
    function MeusEventosPageModule() {
    }
    MeusEventosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__meus_eventos__["a" /* MeusEventosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__meus_eventos__["a" /* MeusEventosPage */]),
            ],
        })
    ], MeusEventosPageModule);
    return MeusEventosPageModule;
}());

//# sourceMappingURL=meus-eventos.module.js.map

/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinhaAgendaPageModule", function() { return MinhaAgendaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__minha_agenda__ = __webpack_require__(298);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MinhaAgendaPageModule = /** @class */ (function () {
    function MinhaAgendaPageModule() {
    }
    MinhaAgendaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__minha_agenda__["a" /* MinhaAgendaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__minha_agenda__["a" /* MinhaAgendaPage */]),
            ],
        })
    ], MinhaAgendaPageModule);
    return MinhaAgendaPageModule;
}());

//# sourceMappingURL=minha-agenda.module.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RedefinirSenhaPageModule", function() { return RedefinirSenhaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__redefinir_senha__ = __webpack_require__(299);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RedefinirSenhaPageModule = /** @class */ (function () {
    function RedefinirSenhaPageModule() {
    }
    RedefinirSenhaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__redefinir_senha__["a" /* RedefinirSenhaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__redefinir_senha__["a" /* RedefinirSenhaPage */]),
            ],
        })
    ], RedefinirSenhaPageModule);
    return RedefinirSenhaPageModule;
}());

//# sourceMappingURL=redefinir-senha.module.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__perfil__ = __webpack_require__(300);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PerfilPageModule = /** @class */ (function () {
    function PerfilPageModule() {
    }
    PerfilPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */]),
            ],
        })
    ], PerfilPageModule);
    return PerfilPageModule;
}());

//# sourceMappingURL=perfil.module.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventoPageModule", function() { return EventoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__evento__ = __webpack_require__(301);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EventoPageModule = /** @class */ (function () {
    function EventoPageModule() {
    }
    EventoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */]),
            ],
        })
    ], EventoPageModule);
    return EventoPageModule;
}());

//# sourceMappingURL=evento.module.js.map

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuarioProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__firebase_firebase__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UsuarioProvider = /** @class */ (function () {
    function UsuarioProvider(firebase) {
        var _this = this;
        this.firebase = firebase;
        this.usuario = {
            uid: '',
            email: '',
            nome: '',
            telefone: '',
            fotoPerfil: '',
            fotoCapa: '',
            bio: '',
            administrador: '',
            gerenciador: ''
        };
        this.firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                _this.usuario.uid = user.uid;
                _this.usuario.email = user.email;
                //Carrega dados do usuário logado
                _this.firebase.db().collection('usuarios').where('uid', '==', _this.usuario.uid).onSnapshot(function (snapshot) {
                    snapshot.docs.forEach(function (doc) {
                        _this.usuario.nome = doc.data().nome;
                        _this.usuario.telefone = doc.data().telefone;
                        _this.usuario.fotoPerfil = doc.data().fotoPerfil;
                        _this.usuario.fotoCapa = doc.data().fotoCapa;
                        _this.usuario.bio = doc.data().bio;
                        _this.usuario.administrador = doc.data().administrador;
                        _this.usuario.gerenciador = doc.data().gerenciador;
                    });
                });
            }
        });
    }
    UsuarioProvider.prototype.get = function () {
        return this.usuario;
    };
    UsuarioProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__firebase_firebase__["a" /* FirebaseProvider */]])
    ], UsuarioProvider);
    return UsuarioProvider;
}());

//# sourceMappingURL=usuario.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(245);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_criar_conta_criar_conta_module__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_editar_evento_editar_evento_module__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_eventos_eventos_module__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_evento_evento_module__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home_module__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_login_login_module__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_mapa_mapa_module__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_meus_eventos_meus_eventos_module__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_minha_agenda_minha_agenda_module__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_perfil_perfil_module__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_redefinir_senha_redefinir_senha_module__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_igrejas_igrejas_module__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_notificacao_notificacao__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_call_number__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_calendar__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_google_maps__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_native_geocoder__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_geolocation__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29_ngx_mask__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_29_ngx_mask__["a" /* NgxMaskModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_7__pages_criar_conta_criar_conta_module__["CriarContaPageModule"],
                __WEBPACK_IMPORTED_MODULE_8__pages_editar_evento_editar_evento_module__["EditarEventoPageModule"],
                __WEBPACK_IMPORTED_MODULE_9__pages_eventos_eventos_module__["EventosPageModule"],
                __WEBPACK_IMPORTED_MODULE_10__pages_evento_evento_module__["EventoPageModule"],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home_module__["HomePageModule"],
                __WEBPACK_IMPORTED_MODULE_12__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_13__pages_mapa_mapa_module__["MapaPageModule"],
                __WEBPACK_IMPORTED_MODULE_14__pages_meus_eventos_meus_eventos_module__["MeusEventosPageModule"],
                __WEBPACK_IMPORTED_MODULE_15__pages_minha_agenda_minha_agenda_module__["MinhaAgendaPageModule"],
                __WEBPACK_IMPORTED_MODULE_16__pages_perfil_perfil_module__["PerfilPageModule"],
                __WEBPACK_IMPORTED_MODULE_17__pages_redefinir_senha_redefinir_senha_module__["RedefinirSenhaPageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_igrejas_igrejas_module__["IgrejasPageModule"],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/criar-evento/criar-evento.module#CriarEventoPageModule', name: 'CriarEventoPage', segment: 'criar-evento', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/criar-conta/criar-conta.module#CriarContaPageModule', name: 'CriarContaPage', segment: 'criar-conta', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/criar-post/criar-post.module#CriarPostPageModule', name: 'CriarPostPage', segment: 'criar-post', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/criar/criar.module#CriarPageModule', name: 'CriarPage', segment: 'criar', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/editar-evento/editar-evento.module#EditarEventoPageModule', name: 'EditarEventoPage', segment: 'editar-evento', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/igrejas/igrejas.module#IgrejasPageModule', name: 'IgrejasPage', segment: 'igrejas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mapa/mapa.module#MapaPageModule', name: 'MapaPage', segment: 'mapa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eventos/eventos.module#EventosPageModule', name: 'EventosPage', segment: 'eventos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meus-dados/meus-dados.module#MeusDadosPageModule', name: 'MeusDadosPage', segment: 'meus-dados', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/meus-eventos/meus-eventos.module#MeusEventosPageModule', name: 'MeusEventosPage', segment: 'meus-eventos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/minha-agenda/minha-agenda.module#MinhaAgendaPageModule', name: 'MinhaAgendaPage', segment: 'minha-agenda', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/redefinir-senha/redefinir-senha.module#RedefinirSenhaPageModule', name: 'RedefinirSenhaPage', segment: 'redefinir-senha', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sobre/sobre.module#SobrePageModule', name: 'SobrePage', segment: 'sobre', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/solicitacoes/solicitacoes.module#SolicitacoesPageModule', name: 'SolicitacoesPage', segment: 'solicitacoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'PerfilPage', segment: 'perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/relatorios/relatorios.module#RelatoriosPageModule', name: 'RelatoriosPage', segment: 'relatorios', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/usuarios/usuarios.module#UsuariosPageModule', name: 'UsuariosPage', segment: 'usuarios', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evento/evento.module#EventoPageModule', name: 'EventoPage', segment: 'evento', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_19__providers_usuario_usuario__["a" /* UsuarioProvider */],
                __WEBPACK_IMPORTED_MODULE_20__providers_firebase_firebase__["a" /* FirebaseProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_notificacao_notificacao__["a" /* NotificacaoProvider */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_calendar__["a" /* Calendar */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_geolocation__["a" /* Geolocation */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CriarContaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_notificacao_notificacao__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the CriarContaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CriarContaPage = /** @class */ (function () {
    function CriarContaPage(modal, navCtrl, navParams, alertCtrl, firebase, notificacao) {
        this.modal = modal;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.firebase = firebase;
        this.notificacao = notificacao;
        this.checkConfirm = false;
        this.igrejas = [];
        this.usuario = {
            uid: '',
            email: '',
            senha: '',
            confrimacaoSenha: '',
            nome: '',
            telefone: '',
            fotoPerfil: 'assets/imgs/default-user.jpg',
            fotoCapa: 'assets/imgs/default-banner.jpg',
            bio: '',
            igreja: ''
        };
        this.carregarIgrejas();
    }
    CriarContaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CriarContaPage');
    };
    CriarContaPage.prototype.criarConta = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.auth().createUserWithEmailAndPassword(this.usuario.email, this.usuario.senha).then(function (user) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            var storageRef;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        storageRef = this.firebase.storage().ref('capa/default-banner.jpg');
                                        return [4 /*yield*/, storageRef.getDownloadURL().then(function (url) {
                                                _this.usuario.fotoCapa = url;
                                            })];
                                    case 1:
                                        _a.sent();
                                        //Obtem a url da imagem padrão de perfil
                                        storageRef = this.firebase.storage().ref('perfil/default-user.jpg');
                                        return [4 /*yield*/, storageRef.getDownloadURL().then(function (url) {
                                                _this.usuario.fotoPerfil = url;
                                            })];
                                    case 2:
                                        _a.sent();
                                        //Cadastra dados do usuário registrado
                                        return [4 /*yield*/, this.firebase.db().collection('usuarios').add({
                                                uid: user.user.uid,
                                                nome: this.usuario.nome,
                                                telefone: this.usuario.telefone,
                                                fotoPerfil: this.usuario.fotoPerfil,
                                                fotoCapa: this.usuario.fotoCapa,
                                                bio: this.usuario.bio,
                                                email: this.usuario.email,
                                                igreja: this.igreja,
                                                administrador: false,
                                                gerenciador: false
                                            }).then(function (data) {
                                                _this.idUsuario = data.id;
                                            }).catch(function (error) {
                                                _this.notificacao.adicionarMensagemErro('Erro ao realizar cadastro!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                            })];
                                    case 3:
                                        //Cadastra dados do usuário registrado
                                        _a.sent();
                                        if (!this.checkConfirm) return [3 /*break*/, 5];
                                        return [4 /*yield*/, this.firebase.db().collection("solicitacao").add({
                                                idUser: this.idUsuario,
                                                nome: this.usuario.nome,
                                                igreja: this.igreja,
                                                data: Date.now(),
                                                analise: false
                                            }).then(function () { }).catch(function (error) {
                                                _this.notificacao.adicionarMensagemErro('Erro ao realizar cadastro!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                            })];
                                    case 4:
                                        _a.sent();
                                        _a.label = 5;
                                    case 5: return [2 /*return*/];
                                }
                            });
                        }); }).catch(function (error) {
                            _this.notificacao.adicionarMensagemErro('Erro ao realizar cadastro!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CriarContaPage.prototype.login = function () {
        this.navCtrl.setRoot('LoginPage');
    };
    CriarContaPage.prototype.abrirInformacao = function () {
        this.modal.create('SobrePage').present();
    };
    CriarContaPage.prototype.carregarIgrejas = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.firebase.db().collection("igreja").onSnapshot(function (snapshot) {
                    _this.igrejas = [];
                    snapshot.docs.forEach(function (doc) {
                        _this.igrejas.push(__assign({ id: doc.id }, doc.data()));
                    });
                });
                return [2 /*return*/];
            });
        });
    };
    CriarContaPage.prototype.confirm = function () {
        var _this = this;
        if (this.checkConfirm) {
            var alert_1 = this.alertCtrl.create({
                title: 'Confirme a sua solicitação',
                message: 'A sua solicitação será enviada ao administrador do aplicativo, onde será analisada e podendo ser recusada.\n Deseja prosseguir com a sua solicitação?',
                buttons: [
                    {
                        text: 'Não',
                        role: 'não',
                        handler: function () {
                            _this.checkConfirm = false;
                        }
                    },
                    {
                        text: 'Sim',
                        role: 'sim'
                    }
                ]
            });
            alert_1.present();
        }
    };
    CriarContaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-criar-conta',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/criar-conta/criar-conta.html"*/'<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header no-border>\n\n  <ion-toolbar transparent>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only clear (click)="abrirInformacao()">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="login">\n\n\n\n  <div class="logo">\n\n    <img src="assets/imgs/logo.png">\n\n  </div>\n\n\n\n  <h3 magin-bottom>Realize seu Cadastro.</h3>\n\n\n\n  <ion-list margin-top margin-top margin-bottom class="list-conta">\n\n    <ion-item>\n\n      <ion-label stacked>Nome</ion-label>\n\n      <ion-input [(ngModel)]="usuario.nome"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        <ion-label stacked>Bio</ion-label>\n\n        <ion-textarea [(ngModel)]="usuario.bio" rows="4"></ion-textarea>\n\n      </ion-item>\n\n    <ion-item padding-bottom>\n\n      <ion-label stacked>Celular</ion-label>\n\n      <ion-input [(ngModel)]="usuario.telefone" type="number" maxlength="13" min="13"></ion-input>\n\n    </ion-item>\n\n\n\n\n\n    <ion-item padding-top>\n\n      <ion-label stacked>Email</ion-label>\n\n      <ion-input [(ngModel)]="usuario.email"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>A qual igreja você pertence?</ion-label>\n\n      <ion-select [(ngModel)]="igreja">\n\n        <ion-option *ngFor="let ieq of igrejas" [value]="ieq.nome">{{ieq.nome}} IEQ</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-checkbox color="dark" (ionChange)="confirm()" [(ngModel)]="checkConfirm" checked="checkConfirm"></ion-checkbox>\n\n      <ion-label>Você deseja criar e gerenciar eventos?</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label stacked>Senha</ion-label>\n\n      <ion-input type="password" [(ngModel)]="usuario.senha"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label stacked>Confirmação Senha</ion-label>\n\n      <ion-input type="password" [(ngModel)]="usuario.confirmacaoSenha"></ion-input>\n\n    </ion-item>\n\n    <button ion-button full round (click)="criarConta()">\n\n      Criar Conta\n\n    </button>\n\n    <button clear full ion-button (click)="login()">\n\n      Ir para o Login\n\n    </button>\n\n  </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/criar-conta/criar-conta.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_notificacao_notificacao__["a" /* NotificacaoProvider */]])
    ], CriarContaPage);
    return CriarContaPage;
}());

//# sourceMappingURL=criar-conta.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditarEventoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_native_geocoder__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(109);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the EditarEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditarEventoPage = /** @class */ (function () {
    function EditarEventoPage(http, geocoder, notificacao, firebase, camera, navCtrl, navParams, view, usuario) {
        this.http = http;
        this.geocoder = geocoder;
        this.notificacao = notificacao;
        this.firebase = firebase;
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.usuario = usuario;
        this.estados = [];
        this.cidades = [];
        this.foto = '';
        this.dados = this.navParams.get('evento');
        this.carregarEstados(false);
        this.carregarCidades(false);
        this.obterDadosCidade();
    }
    EditarEventoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditarEventoPage');
    };
    EditarEventoPage.prototype.cancelar = function () {
        this.dados = this.navParams.get('evento');
        this.foto = '';
    };
    EditarEventoPage.prototype.alterarImagem = function (tipo) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 800,
            targetHeight: 800,
            correctOrientation: true
        };
        if (tipo == 'camera') {
            options.sourceType = this.camera.PictureSourceType.CAMERA;
        }
        else {
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        }
        this.camera.getPicture(options).then(function (imageData) {
            _this.foto = imageData;
            _this.dados.foto = 'data:image/jpeg;base64,' + imageData;
        }, function (err) { });
    };
    EditarEventoPage.prototype.obterCoordenadas = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var endereco;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        endereco = this.dados.endereco.rua + ' ' + this.dados.endereco.numero + ' ' + this.dados.endereco.cidade + ' ' + this.dados.endereco.estado + ' ' + this.dados.endereco.cep;
                        return [4 /*yield*/, this.geocoder.forwardGeocode(endereco)
                                .then(function (coordinates) {
                                _this.dados.coordenadas.latitude = coordinates[0].latitude;
                                _this.dados.coordenadas.longitude = coordinates[0].longitude;
                            }).catch(function (erro) {
                                _this.notificacao.adicionarMensagemErro('Não foi possível salvar as coordenadas do evento. O evento não será exibido no mapa!', _this.notificacao.BOTTOM);
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditarEventoPage.prototype.salvar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var storageRef_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.notificacao.mostrarMensagemAguarde('Aguarde a confirmação ...', this.notificacao.BOTTOM);
                        if (!(this.foto.length > 0)) return [3 /*break*/, 2];
                        storageRef_1 = this.firebase.storage().ref('eventos/' + this.dados.id + '.jpg');
                        return [4 /*yield*/, storageRef_1.putString(this.foto, 'base64', { contentType: 'image/jpeg' }).then(function () { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, storageRef_1.getDownloadURL().then(function (url) { return __awaiter(_this, void 0, void 0, function () {
                                                return __generator(this, function (_a) {
                                                    this.dados.foto = url;
                                                    this.foto = '';
                                                    return [2 /*return*/];
                                                });
                                            }); }).catch(function (erro) {
                                                _this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', _this.notificacao.BOTTOM);
                                                _this.foto = '';
                                            })];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [4 /*yield*/, this.obterCoordenadas()];
                    case 3:
                        _a.sent();
                        //Salva dados do evento
                        return [4 /*yield*/, this.firebase.db().collection('eventos').doc(this.dados.id).update(__assign({}, this.dados, { dataAlteracao: new Date().toJSON().split('T')[0] })).then(function (doc) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    this.notificacao.adicionarMensagemSucesso('Evento editado com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
                                    this.fechar();
                                    return [2 /*return*/];
                                });
                            }); }).catch(function (error) {
                                _this.notificacao.adicionarMensagemErro('Erro ao editar evento!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                            })];
                    case 4:
                        //Salva dados do evento
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EditarEventoPage.prototype.fechar = function () {
        this.view.dismiss();
    };
    //Carrega lista de estados para o campo estado (select)
    EditarEventoPage.prototype.carregarEstados = function (selectDefault) {
        var _this = this;
        var url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados';
        this.http.get(url).subscribe(function (data) {
            var result = data.json();
            _this.estados = [];
            if (selectDefault) {
                _this.dados.endereco.cidade = result[0].id;
            }
            result.forEach(function (estado) {
                _this.estados.push({
                    id: estado.id,
                    nome: estado.nome,
                    sigla: estado.sigla
                });
            });
        });
    };
    //Carrega lista de cidades para o campo cidade (select) de acordo com o estado selecionado
    EditarEventoPage.prototype.carregarCidades = function (selectDefault) {
        var _this = this;
        var url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/" + this.dados.endereco.idEstado + "/municipios";
        this.http.get(url).subscribe(function (data) {
            var result = data.json();
            _this.cidades = [];
            if (selectDefault) {
                _this.dados.endereco.cidade = result[0].id;
            }
            result.forEach(function (cidade) {
                _this.cidades.push({
                    id: cidade.id,
                    nome: cidade.nome
                });
            });
        });
    };
    //Obtem informações da cidade selecionada (nome da cidade e sigla do estado)
    EditarEventoPage.prototype.obterDadosCidade = function () {
        var _this = this;
        var url = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios/" + this.dados.endereco.idCidade;
        this.http.get(url).subscribe(function (data) {
            var result = data.json();
            _this.dados.endereco.estado = result.microrregiao.mesorregiao.UF.sigla;
            _this.dados.endereco.cidade = result.nome;
        });
    };
    EditarEventoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-editar-evento',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/editar-evento/editar-evento.html"*/'<!--\n\n  Generated template for the EditarEventoPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar color="dark">\n\n    <ion-buttons left>\n\n      <button ion-button class="icon-large">\n\n        <ion-icon name="create"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Editar Evento</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button class="icon-large" (click)="fechar()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="image">\n\n    <ion-fab top right>\n\n      <button ion-fab color="light" mini>\n\n        <ion-icon name="create"></ion-icon>\n\n      </button>\n\n      <ion-fab-list side="bottom">\n\n        <button ion-fab mini (click)="alterarImagem(\'arquivo\')">\n\n          <ion-icon name="image"></ion-icon>\n\n        </button>\n\n        <button ion-fab mini (click)="alterarImagem(\'camera\')">\n\n          <ion-icon name="camera"></ion-icon>\n\n        </button>\n\n      </ion-fab-list>\n\n    </ion-fab>\n\n    <img [src]="dados.foto">\n\n  </div>\n\n  <div class="form">\n\n    <ion-item>\n\n      <ion-label floating>Nome</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.nome"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Descrição</ion-label>\n\n      <ion-textarea type="text" rows="4" [(ngModel)]="dados.descricao"></ion-textarea>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Data</ion-label>\n\n      <ion-input type="date" [(ngModel)]="dados.data"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Horário</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.horario"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-top>\n\n      <ion-label floating>Estado</ion-label>\n\n      <ion-select [(ngModel)]="dados.endereco.idEstado" (ionChange)="carregarCidades(true)">\n\n        <ion-option *ngFor="let estado of estados, let i = index" [value]="estado.id" [selected]="this.dados.endereco.estado == estado.id" (click)="salvarEstado(estado)">{{ estado.nome }}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Cidade</ion-label>\n\n      <ion-select [(ngModel)]="dados.endereco.idCidade" (ionChange)="obterDadosCidade()">\n\n        <ion-option *ngFor="let cidade of cidades, let i = index" [value]="cidade.id" [selected]="this.dados.endereco.cidade == cidade.id" (click)="salvarCidade(cidade)">{{ cidade.nome }}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Bairro</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.bairro"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Rua</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.rua"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Número</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.numero"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>CEP</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.cep"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-bottom>\n\n      <ion-label floating>Referência</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.referencia"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-top>\n\n      <ion-label floating>Nome do Contato para Informações</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.nomeContato"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Email para Contato</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.emailContato"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-bottom>\n\n      <ion-label floating>Telefone/Whatsapp para Contato</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.telefoneContato"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-top>\n\n      <ion-label floating>Informações Adicionais</ion-label>\n\n      <ion-textarea type="text" rows="4" [(ngModel)]="dados.informacoesAdicionais"></ion-textarea>\n\n    </ion-item>\n\n  </div>\n\n  <div class="botoes">\n\n    <button ion-button full round (click)="salvar()">Salvar</button>\n\n    <button clear full ion-button (click)="cancelar()">Cancelar</button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/editar-evento/editar-evento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__["a" /* NotificacaoProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */]])
    ], EditarEventoPage);
    return EditarEventoPage;
}());

//# sourceMappingURL=editar-evento.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomePage = /** @class */ (function () {
    function HomePage(usuario, firebase, events, navCtrl, navParams, alertCtrl) {
        var _this = this;
        this.usuario = usuario;
        this.firebase = firebase;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.homeTab = 'EventosPage';
        this.mapaTab = 'MapaPage';
        this.agendaTab = 'MinhaAgendaPage';
        this.filtro = {
            dataInicial: '',
            dataFinal: '',
            texto: ''
        };
        this.solicitacoes = 0;
        this.titulo = 'Home';
        this.tab = 'home';
        this.firebase.db().collection('solicitacao').where("analise", "==", false)
            .onSnapshot(function (snapshot) {
            snapshot.docs.forEach(function (doc) {
                _this.solicitacoes++;
            });
        });
    }
    HomePage.prototype.placeholder = function () {
        return this.tab == 'mapa' ? 'Local ou Região' : 'Nome ou Descrição';
    };
    HomePage.prototype.contarSolicitacoes = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    HomePage.prototype.carregarEventos = function () {
        this.events.publish('filtrar-' + this.tab);
    };
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.alterarTab = function (tab) {
        this.tab = tab;
        switch (tab) {
            case 'home':
                this.titulo = 'Home';
                break;
            case 'mapa':
                this.titulo = 'Mapa de Eventos';
                break;
            case 'agenda':
                this.titulo = 'Minha Agenda';
                break;
        }
    };
    HomePage.prototype.buscarPorData = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Pesquisar Por Período',
            inputs: [
                {
                    name: 'dataInicial',
                    placeholder: 'Data Inicial',
                    type: 'date',
                    value: this.filtro.dataInicial
                },
                {
                    name: 'dataFinal',
                    placeholder: 'Data Final',
                    type: 'date',
                    value: this.filtro.dataFinal
                }
            ],
            buttons: [
                {
                    text: 'Limpar',
                    role: 'limpar',
                    handler: function (data) {
                        _this.filtro.dataInicial = '';
                        _this.filtro.dataFinal = '';
                        _this.carregarEventos();
                    }
                },
                {
                    text: 'Pesquisar',
                    handler: function (data) {
                        _this.filtro.dataInicial = data.dataInicial;
                        _this.filtro.dataFinal = data.dataFinal;
                        _this.carregarEventos();
                    }
                }
            ]
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('searchbar'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Searchbar */])
    ], HomePage.prototype, "searchbar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('tabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Tabs */])
    ], HomePage.prototype, "tabs", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/home/home.html"*/'<!--\n\n  Generated template for the TabsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar color="dark">\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n        <p class="aviso" *ngIf="solicitacoes > 0 && usuario.get().administrador==true">{{solicitacoes}}</p>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{ titulo }}</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button class="icon-med" (click)="buscarPorData()">\n\n        <ion-icon name="calendar"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-tabs color="dark" #tabs class="tab">\n\n    <ion-tab [root]="homeTab" [rootParams]="filtro" tabTitle="Home" tabIcon="home" (ionSelect)="alterarTab(\'home\')"></ion-tab>\n\n    <ion-tab [root]="mapaTab" [rootParams]="filtro" tabTitle="Mapa" tabIcon="map" (ionSelect)="alterarTab(\'mapa\')"></ion-tab>\n\n    <ion-tab [root]="agendaTab" [rootParams]="filtro" tabTitle="Minha Agenda" tabIcon="bookmarks" (ionSelect)="alterarTab(\'agenda\')"></ion-tab>\n\n  </ion-tabs>\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(modal, formeBuilder, navCtrl, navParams, alertCtrl, firebase, notificacao) {
        this.modal = modal;
        this.formeBuilder = formeBuilder;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.firebase = firebase;
        this.notificacao = notificacao;
        this.logar = {
            email: '',
            senha: ''
        };
        this.formGroup = this.formeBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            senha: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
        this.email = this.formGroup.controls['email'];
        this.senha = this.formGroup.controls['senha'];
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        if (this.email.hasError('required')) {
                            this.notificacao.adicionarMensagemErro('E-mail é obrigatório!', this.notificacao.BOTTOM).mostrarNotificacoes();
                            return [2 /*return*/];
                        }
                        else if (this.senha.hasError('required')) {
                            this.notificacao.adicionarMensagemErro('Senha é obrigatória!', this.notificacao.BOTTOM).mostrarNotificacoes();
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.firebase.auth().signInWithEmailAndPassword(this.logar.email, this.logar.senha)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        this.notificacao.adicionarMensagemErro('E-mail ou senha incorretos!', this.notificacao.BOTTOM).mostrarNotificacoes();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.criarConta = function () {
        this.navCtrl.push('CriarContaPage');
    };
    LoginPage.prototype.redefinirSenha = function () {
        this.navCtrl.push('RedefinirSenhaPage');
    };
    LoginPage.prototype.abrirInformacao = function () {
        this.modal.create('SobrePage').present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/login/login.html"*/'<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header no-border>\n\n    <ion-toolbar transparent>\n\n      <ion-buttons end> \n\n        <button ion-button icon-only clear (click)="abrirInformacao()">\n\n          <ion-icon name="more"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n<ion-content padding class="login">\n\n\n\n    <div class="logo">\n\n      <img src="assets/imgs/logo.png">\n\n    </div>\n\n\n\n    <h3 magin-bottom>Acesse sua conta.</h3>\n\n    <form [formGroup]="formGroup" class="form-login">\n\n      <ion-list margin-top margin-top margin-bottom>\n\n        <ion-item>\n\n          <ion-label stacked>Nome de usuário ou Email</ion-label>\n\n          <ion-input type="text" formControlName="email"[(ngModel)]="logar.email"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label stacked>Senha</ion-label>\n\n          <ion-input type="password" formControlName="senha" [(ngModel)]="logar.senha"></ion-input>\n\n        </ion-item>\n\n        \n\n        <button ion-button secondary round full (click)="login()">Entrar</button>\n\n        <button ion-button full clear small (click)="redefinirSenha()">Esqueci minha senha</button>\n\n        <h6 text-right class="btn-conta" rigth (click)="criarConta()">Não possui uma conta?</h6>\n\n      </ion-list>\n\n    </form>   \n\n</ion-content>\n\n\n\n'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IgrejasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the IgrejasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IgrejasPage = /** @class */ (function () {
    function IgrejasPage(navCtrl, navParams, alertCtrl, modal, usuario, firebase, notificacao) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modal = modal;
        this.usuario = usuario;
        this.firebase = firebase;
        this.notificacao = notificacao;
        this.igrejas = [];
        this.membros = 0;
        this.countIgrejas = 0;
        this.novaIgreja = 0;
        this.carregarIgrejas();
    }
    IgrejasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IgrejasPage');
    };
    IgrejasPage.prototype.carregarIgrejas = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.db().collection("igreja").orderBy("nome").onSnapshot(function (snapshot) {
                            snapshot.docs.forEach(function (doc) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            this.igrejas = [];
                                            return [4 /*yield*/, this.firebase.db().collection("usuarios").where("igreja", "==", doc.data().nome).get().then(function (result) {
                                                    _this.membros = result.docs.length;
                                                })];
                                        case 1:
                                            _a.sent();
                                            this.igrejas.push(__assign({ id: doc.id }, doc.data(), { membros: this.membros }));
                                            this.countIgrejas++;
                                            this.novaIgreja = this.countIgrejas + 1;
                                            return [2 /*return*/];
                                    }
                                });
                            }); });
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    IgrejasPage.prototype.newIgreja = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.db().collection("igreja").add({
                            nome: this.novaIgreja,
                        })];
                    case 1:
                        _a.sent();
                        this.carregarIgrejas();
                        return [2 /*return*/];
                }
            });
        });
    };
    IgrejasPage.prototype.addIgreja = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirme',
            message: 'Tem certeza que deseja incluir a ' + this.novaIgreja + ' IEQ?',
            buttons: [
                {
                    text: 'Não',
                    role: 'não',
                },
                {
                    text: 'Sim',
                    role: 'sim',
                    handler: function () {
                        _this.newIgreja();
                    }
                }
            ]
        });
        alert.present();
    };
    IgrejasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-igrejas',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/igrejas/igrejas.html"*/'<!--\n  Generated template for the IgrejasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="dark">\n    <ion-buttons left>\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n      </ion-buttons>\n  <ion-title>Igrejas</ion-title>\n  <ion-buttons right>\n    <button ion-button class="icon-med" (click)="addIgreja()">\n      <ion-icon name="add"></ion-icon>\n    </button>\n\n  </ion-buttons>\n</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item *ngFor="let igreja of igrejas">\n      <h2>{{igreja.nome}} IEQ</h2>\n      <p>{{igreja.membros}} membros</p>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/igrejas/igrejas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */]])
    ], IgrejasPage);
    return IgrejasPage;
}());

//# sourceMappingURL=igrejas.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_notificacao_notificacao__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MapaPage = /** @class */ (function () {
    function MapaPage(alertCtrl, notificacao, geolocation, events, navCtrl, navParams, firebase, usuario, modal) {
        var _this = this;
        this.alertCtrl = alertCtrl;
        this.notificacao = notificacao;
        this.geolocation = geolocation;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firebase = firebase;
        this.usuario = usuario;
        this.modal = modal;
        this.eventos = [];
        this.unsubscribe = function () { };
        this.filtro = {
            dataInicial: '',
            dataFinal: '',
            texto: ''
        };
        this.coordenadas = { latitude: 0, longitude: 0 };
        this.filtro = this.navParams.data;
        this.obterCoordendas();
        events.subscribe('filtrar-mapa', function () {
            _this.carregarEventos();
        });
    }
    MapaPage.prototype.obterCoordendas = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (posicao) {
            _this.coordenadas.latitude = posicao.coords.latitude;
            _this.coordenadas.longitude = posicao.coords.longitude;
            _this.iniciarMapa();
        }).catch(function (erro) {
            _this.iniciarMapa();
        });
    };
    MapaPage.prototype.iniciarMapa = function () {
        var mapOptions = {
            mapType: "MAP_TYPE_ROADMAP",
            camera: {
                target: {
                    lat: this.coordenadas.latitude,
                    lng: this.coordenadas.longitude
                },
                zoom: 10
            }
        };
        this.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas', mapOptions);
        this.carregarEventos();
    };
    MapaPage.prototype.carregarEventos = function () {
        var _this = this;
        this.unsubscribe();
        var ref = this.firebase.db().collection('eventos')
            .orderBy('data', 'desc');
        if (this.filtro.dataInicial != undefined && this.filtro.dataInicial.length > 0) {
            ref = ref.where('data', '>=', this.filtro.dataInicial);
        }
        if (this.filtro.dataFinal != undefined && this.filtro.dataFinal.length > 0) {
            ref = ref.where('data', '<=', this.filtro.dataFinal);
        }
        this.unsubscribe = ref.onSnapshot(function (snapshot) {
            _this.eventos.forEach(function (marker) {
                if (marker != null && marker != undefined)
                    marker.remove();
            });
            _this.eventos = [];
            snapshot.docs.forEach(function (doc) {
                var evento = __assign({ id: doc.id }, doc.data());
                _this.criarMarcador(evento);
            });
        });
    };
    MapaPage.prototype.criarMarcador = function (evento) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var marker;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(evento.coordenadas.latitude && evento.coordenadas.longitude)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.map.addMarkerSync({
                                title: evento.nome,
                                icon: 'red',
                                animation: 'DROP',
                                position: {
                                    lat: parseFloat(evento.coordenadas.latitude),
                                    lng: parseFloat(evento.coordenadas.longitude)
                                }
                            })];
                    case 1:
                        marker = _a.sent();
                        marker.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                            var eventoModal = _this.modal.create("EventoPage", { evento: evento, abrirPerfil: true });
                            eventoModal.present();
                        });
                        this.eventos.push(marker);
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    MapaPage.prototype.abrirDetalhes = function (evento) {
        this.modal.create("EventoPage", {
            evento: evento,
            abrirPerfil: true
        }).present();
    };
    MapaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapaPage');
    };
    MapaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mapa',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/mapa/mapa.html"*/'<!--\n\n  Generated template for the MapaPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-content>\n\n  <div id="map_canvas"></div>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/mapa/mapa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_6__providers_notificacao_notificacao__["a" /* NotificacaoProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], MapaPage);
    return MapaPage;
}());

//# sourceMappingURL=mapa.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the EventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EventosPage = /** @class */ (function () {
    function EventosPage(notificacao, events, navCtrl, navParams, firebase, usuario, modal) {
        var _this = this;
        this.notificacao = notificacao;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firebase = firebase;
        this.usuario = usuario;
        this.modal = modal;
        this.eventos = [];
        this.unsubscribe = function () { };
        this.filtro = {
            dataInicial: '',
            dataFinal: '',
            texto: ''
        };
        this.filtro = this.navParams.data;
        this.carregarEventos();
        events.subscribe('filtrar-eventos', function () {
            _this.carregarEventos();
        });
    }
    EventosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventosPage');
    };
    EventosPage.prototype.carregarEventos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var ref;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.unsubscribe();
                        return [4 /*yield*/, this.firebase.db().collection('eventos').orderBy('data', 'desc')];
                    case 1:
                        ref = _a.sent();
                        if (this.filtro.dataInicial != undefined && this.filtro.dataInicial.length > 0) {
                            ref = ref.where('data', '>=', this.filtro.dataInicial);
                        }
                        if (this.filtro.dataFinal != undefined && this.filtro.dataFinal.length > 0) {
                            ref = ref.where('data', '<=', this.filtro.dataFinal);
                        }
                        this.unsubscribe = ref.onSnapshot(function (snapshot) {
                            _this.eventos = [];
                            snapshot.docs.forEach(function (doc) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, this.firebase.db().collection('usuarios').where('uid', '==', doc.data().idUsuario).get().then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            if (!(result.docs.length > 0)) return [3 /*break*/, 2];
                                                            return [4 /*yield*/, this.eventos.push(__assign({ id: doc.id }, doc.data(), { usuario: __assign({}, result.docs[0].data()) }))];
                                                        case 1:
                                                            _a.sent();
                                                            return [3 /*break*/, 4];
                                                        case 2: return [4 /*yield*/, this.eventos.push(__assign({ id: doc.id }, doc.data(), { usuario: { fotoPerfil: './../../assets/imgs/default-user.jpg', nome: 'Sem Identificação' } }))];
                                                        case 3:
                                                            _a.sent();
                                                            _a.label = 4;
                                                        case 4: return [2 /*return*/];
                                                    }
                                                });
                                            }); }).catch(function (erro) { })];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    EventosPage.prototype.abrirDetalhes = function (evento) {
        this.modal.create("EventoPage", {
            evento: evento,
            abrirPerfil: true
        }).present();
    };
    EventosPage.prototype.abrirPerfil = function (usuario) {
        this.modal.create('PerfilPage', {
            usuario: usuario
        }).present();
    };
    EventosPage.prototype.participar = function (icon, evento) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(icon == "hand")) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.firebase.db().collection("participar").add({
                                idUser: this.usuario.get().uid,
                                idEvento: evento.id,
                                data: new Date().toJSON().split('T')[0]
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.firebase.db().collection("participar").doc(evento.idParticipar).delete()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4:
                        this.carregarEventos();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-eventos',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/eventos/eventos.html"*/'<!--\n\n  Generated template for the EventosPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<meta charset="UTF-8">\n\n<ion-content class="eventos">\n\n  <h3 *ngIf="eventos.length == 0" class="sem-registros">\n\n    Não foi encontrado nenhum evento!\n\n  </h3>\n\n  <ion-card class="card-event" *ngFor="let evento of eventos">\n\n    <ion-item *ngIf="evento.usuario" (click)="abrirPerfil(evento.usuario)">\n\n      <ion-avatar item-start>\n\n        <img src="{{ evento.usuario.fotoPerfil }}">\n\n      </ion-avatar>\n\n      <h2>{{ evento.usuario.nome }}</h2>\n\n      <ion-note>\n\n        Publicado em {{ evento.dataPublicacao | date:\'dd/MM/yyyy\' }}\n\n      </ion-note>   \n\n    </ion-item>\n\n    <img src="{{ evento.foto }}">\n\n    <ion-card-content>\n\n      <ion-col class="data-evento">\n\n        <span>{{ evento.data | date:\'dd/MM/yyyy\'}}</span>\n\n      </ion-col>\n\n      <h1 class="titulo">{{ evento.nome }}</h1>\n\n      <p>{{ evento.descricao }}</p>\n\n    </ion-card-content>\n\n    <ion-row>\n\n      <ion-col class="btn-action">\n\n        <button ion-button icon-left clear small (click)="abrirDetalhes(evento)">\n\n          <ion-icon name="information-circle"></ion-icon>\n\n          Mais Informações\n\n        </button>\n\n      </ion-col>\n\n      \n\n    </ion-row>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/eventos/eventos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], EventosPage);
    return EventosPage;
}());

//# sourceMappingURL=eventos.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeusEventosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the MeusEventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MeusEventosPage = /** @class */ (function () {
    function MeusEventosPage(navCtrl, navParams, alertCtrl, modal, usuario, firebase, notificacao) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modal = modal;
        this.usuario = usuario;
        this.firebase = firebase;
        this.notificacao = notificacao;
        this.eventos = [];
        this.dataInicial = '';
        this.dataFinal = '';
        this.unsubscribe = function () { };
        this.carregarEventos();
    }
    MeusEventosPage.prototype.carregarEventos = function () {
        var _this = this;
        this.unsubscribe();
        var ref = this.firebase.db().collection('eventos').
            where('idUsuario', '==', this.usuario.get().uid);
        if (this.dataInicial.length > 0) {
            ref = ref.where('data', '>=', this.dataInicial);
        }
        if (this.dataFinal.length > 0) {
            ref = ref.where('data', '<=', this.dataFinal);
        }
        ref = ref.orderBy('data', 'desc');
        this.unsubscribe = ref.onSnapshot(function (snapshot) {
            _this.eventos = [];
            snapshot.docs.forEach(function (doc) {
                _this.eventos.push(__assign({ id: doc.id }, doc.data()));
            });
        });
    };
    MeusEventosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MeusEventosPage');
    };
    MeusEventosPage.prototype.criarEvento = function () {
        this.modal.create("CriarEventoPage").present();
    };
    MeusEventosPage.prototype.editarEvento = function (evento) {
        this.modal.create("EditarEventoPage", {
            evento: evento
        }).present();
    };
    MeusEventosPage.prototype.excluirEvento = function (evento) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Excluir Evento',
            subTitle: 'Deseja realmente excluir o evento ' + evento.nome + ' na data ' + evento.data + '?',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancelar',
                    handler: function (data) { }
                },
                {
                    text: 'Excluir',
                    handler: function (data) {
                        _this.notificacao.mostrarMensagemAguarde('Aguarde ...', _this.notificacao.BOTTOM);
                        _this.firebase.db().collection('eventos').doc(evento.id).delete().then(function () {
                            _this.firebase.db().collection('eventos').where('idEvento', '==', evento.id).get().then(function (result) {
                                result.docs.forEach(function (doc) {
                                    _this.firebase.db().collection('agenda').doc(doc.id).delete();
                                });
                            });
                            _this.notificacao.adicionarMensagemSucesso('Evento excluído com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        }).catch(function (erro) {
                            _this.notificacao.adicionarMensagemErro('Erro ao excluir evento!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    MeusEventosPage.prototype.buscarPorData = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Pesquisar Por Período',
            inputs: [
                {
                    name: 'dataInicial',
                    placeholder: 'Data Inicial',
                    type: 'date',
                    value: this.dataInicial
                },
                {
                    name: 'dataFinal',
                    placeholder: 'Data Final',
                    type: 'date',
                    value: this.dataFinal
                }
            ],
            buttons: [
                {
                    text: 'Limpar',
                    role: 'limpar',
                    handler: function (data) {
                        _this.dataInicial = '';
                        _this.dataFinal = '';
                        _this.carregarEventos();
                    }
                },
                {
                    text: 'Pesquisar',
                    handler: function (data) {
                        _this.dataInicial = data.dataInicial;
                        _this.dataFinal = data.dataFinal;
                        _this.carregarEventos();
                    }
                }
            ]
        });
        alert.present();
    };
    MeusEventosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-meus-eventos',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/meus-eventos/meus-eventos.html"*/'<!--\n\n  Generated template for the MeusEventosPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar color="dark">\n\n    <ion-buttons left>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Meus Eventos</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button class="icon-med" (click)="buscarPorData()">\n\n        <ion-icon name="calendar"></ion-icon>\n\n      </button>\n\n      <button ion-button class="icon-med" (click)="criarEvento()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content class="eventos">\n\n  <h3 *ngIf="eventos.length == 0" class="sem-registros">\n\n    Nenhum evento encontrado!\n\n  </h3>\n\n  <ion-card *ngFor="let evento of eventos">\n\n    <img src="{{ evento.foto }}">\n\n    <ion-card-content>\n\n      <h1 class="titulo">{{ evento.nome }}</h1>\n\n      <p>{{ evento.descricao }}</p>\n\n      <!-- <ion-note>\n\n        11h ago\n\n      </ion-note> -->\n\n    </ion-card-content>\n\n    <ion-row>\n\n      <ion-col>\n\n        <button ion-button icon-left clear small (click)="editarEvento(evento)">\n\n          <ion-icon name="create"></ion-icon>\n\n          Editar\n\n        </button>\n\n        <button ion-button icon-left clear small (click)="excluirEvento(evento)">\n\n          <ion-icon name="trash"></ion-icon>\n\n          Excluir\n\n        </button>\n\n      </ion-col>\n\n      <ion-col class="data">\n\n        <span>{{ evento.dataPublicacao | date:\'dd/MM/yyyy\' }}</span>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/meus-eventos/meus-eventos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */]])
    ], MeusEventosPage);
    return MeusEventosPage;
}());

//# sourceMappingURL=meus-eventos.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MinhaAgendaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__ = __webpack_require__(18);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MinhaAgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MinhaAgendaPage = /** @class */ (function () {
    function MinhaAgendaPage(events, navCtrl, navParams, firebase, usuario, modal) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.firebase = firebase;
        this.usuario = usuario;
        this.modal = modal;
        this.eventos = [];
        this.unsubscribe = function () { };
        this.filtro = this.navParams.data;
        this.carregarEventos();
        events.subscribe('filtrar-agenda', function () {
            _this.carregarEventos();
        });
    }
    MinhaAgendaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MinhaAgendaPage');
    };
    MinhaAgendaPage.prototype.carregarEventos = function () {
        var _this = this;
        this.unsubscribe();
        this.unsubscribe = this.firebase.db().collection('agenda').
            where('idUsuario', '==', this.usuario.get().uid).
            onSnapshot(function (snapshot) {
            _this.eventos = [];
            snapshot.docs.forEach(function (doc) {
                _this.firebase.db().collection('eventos').doc(doc.data().idEvento).get().then(function (evento) {
                    if (evento.exists) {
                        if (_this.filtro.dataInicial != undefined && _this.filtro.dataInicial.length > 0) {
                            if (Date.parse(evento.data().data) < Date.parse(_this.filtro.dataInicial)) {
                                return;
                            }
                        }
                        if (_this.filtro.dataFinal != undefined && _this.filtro.dataFinal.length > 0) {
                            if (Date.parse(evento.data().data) > Date.parse(_this.filtro.dataFinal)) {
                                return;
                            }
                        }
                        _this.eventos.push(__assign({ id: evento.id }, evento.data()));
                    }
                });
            });
        });
    };
    MinhaAgendaPage.prototype.abrirDetalhes = function (evento) {
        this.modal.create("EventoPage", {
            evento: evento
        }).present();
    };
    MinhaAgendaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-minha-agenda',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/minha-agenda/minha-agenda.html"*/'<!--\n\n  Generated template for the MinhaAgendaPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n<ion-content class="agenda" padding>\n\n  <h3 *ngIf="eventos.length == 0" class="sem-registros">\n\n      Nenhum evento encontrado!\n\n  </h3>\n\n  <ion-list>\n\n    <button ion-item (click)="abrirDetalhes(evento)" *ngFor="let evento of eventos">\n\n      <h1 class="titulo">{{ evento.nome }}</h1>\n\n      <p>{{ evento.descricao }}</p>\n\n      <ion-note item-right>{{ evento.data | date:\'dd/MM/yyyy\' }}</ion-note>\n\n    </button>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/minha-agenda/minha-agenda.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], MinhaAgendaPage);
    return MinhaAgendaPage;
}());

//# sourceMappingURL=minha-agenda.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RedefinirSenhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_notificacao_notificacao__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RedefinirSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RedefinirSenhaPage = /** @class */ (function () {
    function RedefinirSenhaPage(modal, notificacao, firebase, navCtrl, navParams, alertCtrl) {
        this.modal = modal;
        this.notificacao = notificacao;
        this.firebase = firebase;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.email = '';
    }
    RedefinirSenhaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RedefinirSenhaPage');
    };
    RedefinirSenhaPage.prototype.login = function () {
        this.navCtrl.setRoot('LoginPage');
    };
    RedefinirSenhaPage.prototype.redefinir = function () {
        var _this = this;
        //Envia emial de redefinição de senha //pelo firebase
        this.firebase.auth().sendPasswordResetEmail(this.email).then(function () {
            _this.notificacao.adicionarMensagemSucesso('Email de redefinição de senha enviado com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
        }).catch(function (erro) {
            _this.notificacao.adicionarMensagemErro('Não foi possível enviar o email de redefinição de senha!', _this.notificacao.BOTTOM).mostrarNotificacoes();
        });
    };
    RedefinirSenhaPage.prototype.abrirInformacao = function () {
        this.modal.create('SobrePage').present();
    };
    RedefinirSenhaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-redefinir-senha',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/redefinir-senha/redefinir-senha.html"*/'<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header no-border>\n\n  <ion-navbar color="branco">\n\n    <ion-buttons left>\n\n      <button ion-button class="icon-large" (click)="login()">\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only clear (click)="abrirInformacao()">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-toolbar transparent>\n\n    \n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="login">\n\n\n\n  <div class="logo">\n\n    <img src="assets/imgs/logo.png">\n\n  </div>\n\n\n\n  <h3 magin-bottom>Redefina sua Senha.</h3>\n\n\n\n  <ion-list margin-top margin-top margin-bottom>\n\n    <ion-item>\n\n      <ion-label stacked>Nome de usuário ou Email</ion-label>\n\n      <ion-input type="text" [(ngModel)]="email"></ion-input>\n\n    </ion-item>\n\n    <button ion-button full round (click)="redefinir()">\n\n      Redefinir Senha\n\n    </button>\n\n  </ion-list>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/redefinir-senha/redefinir-senha.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__providers_notificacao_notificacao__["a" /* NotificacaoProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], RedefinirSenhaPage);
    return RedefinirSenhaPage;
}());

//# sourceMappingURL=redefinir-senha.js.map

/***/ }),

/***/ 300:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PerfilPage = /** @class */ (function () {
    function PerfilPage(view, navCtrl, navParams, modal, usuario, firebase, notificacao) {
        this.view = view;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modal = modal;
        this.usuario = usuario;
        this.firebase = firebase;
        this.notificacao = notificacao;
        this.titulo = 'Perfil';
        this.eventos = [];
        if (this.navParams.get('usuario')) {
            this.dados = this.navParams.get('usuario');
            if (this.dados.uid == this.usuario.get().uid) {
                this.titulo = 'Meu Perfil';
            }
            this.carregarEventos(this.dados.uid);
        }
        else {
            this.sair();
        }
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerfilPage');
    };
    PerfilPage.prototype.carregarEventos = function (idUsuario) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.firebase.db().collection('eventos').
                    where('idUsuario', '==', idUsuario).
                    orderBy('dataPublicacao', 'desc').
                    onSnapshot(function (snapshot) {
                    _this.eventos = [];
                    snapshot.docs.forEach(function (doc) {
                        _this.eventos.push(__assign({ id: doc.id }, doc.data()));
                    });
                });
                return [2 /*return*/];
            });
        });
    };
    PerfilPage.prototype.abrirDetalhes = function (evento) {
        this.modal.create("EventoPage", {
            evento: evento,
            abrirPerfil: false
        }).present();
    };
    PerfilPage.prototype.sair = function () {
        this.view.dismiss();
    };
    //Abre conversa de chat no WhatsApp
    PerfilPage.prototype.conversarWhatsapp = function () {
        var url = 'https://api.whatsapp.com/send?phone=55' + this.dados.telefone;
        location.href = url;
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-perfil',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/perfil/perfil.html"*/'<!--\n\n  Generated template for the PerfilPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar color="dark">\n\n    <ion-buttons left>\n\n      <button ion-button class="icon-large">\n\n        <ion-icon name="person"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{ titulo }}</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button class="icon-large" (click)="sair()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="perfil">\n\n    <div class="capa">\n\n      <img src="{{ dados.fotoCapa }}">\n\n      <div class="foto">\n\n        <img src="{{ dados.fotoPerfil }}">\n\n      </div>\n\n    </div>\n\n    <div class="info">\n\n      <div class="dados-pessoais">\n\n        <h1 class="nome">{{ dados.nome }}</h1>\n\n        <p class="bio">{{ dados.bio }}</div>\n\n      <div class="dados-contato">\n\n        <button ion-button icon-left block clear class="email">\n\n          <ion-icon name="mail"></ion-icon>\n\n          {{ dados.email }}\n\n        </button>\n\n        <button ion-button icon-left block clear class="telefone" (click)="conversarWhatsapp()">\n\n          <ion-icon name="logo-whatsapp"></ion-icon>\n\n          {{ dados.telefone }}\n\n        </button>\n\n\n\n      </div>\n\n    </div>\n\n  </div>\n\n  <div class="eventos" *ngIf="usuario.get().gerenciador==true">\n\n    <h3 *ngIf="eventos.length == 0" class="sem-registros">\n\n      Nenhum evento publicado!\n\n    </h3>\n\n    <ion-card *ngFor="let evento of eventos">\n\n      <img src="{{ evento.foto }}">\n\n      <ion-card-content>\n\n        <h1 class="titulo">{{ evento.nome }}</h1>\n\n        <p>{{ evento.descricao }}</p>\n\n        <!-- <ion-note>\n\n          11h ago\n\n        </ion-note> -->\n\n      </ion-card-content>\n\n      <ion-row>\n\n        <ion-col>\n\n          <button ion-button icon-left clear small (click)="abrirDetalhes(evento)">\n\n            <ion-icon name="information-circle"></ion-icon>\n\n            Mais Informações\n\n          </button>\n\n        </ion-col>\n\n        <ion-col class="data">\n\n          <span>{{ evento.dataPublicacao | date:\'dd/MM/yyyy\' }}</span>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n  </div>\n\n\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/perfil/perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_calendar__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_geocoder__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









/**
 * Generated class for the EventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EventoPage = /** @class */ (function () {
    function EventoPage(geocoder, alertCtrl, calendar, callNumber, socialSharing, notificacao, modal, navCtrl, navParams, view, usuario, firebase) {
        this.geocoder = geocoder;
        this.alertCtrl = alertCtrl;
        this.calendar = calendar;
        this.callNumber = callNumber;
        this.socialSharing = socialSharing;
        this.notificacao = notificacao;
        this.modal = modal;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.usuario = usuario;
        this.firebase = firebase;
        this.usuarioPub = {};
        this.podeAbrirPefil = true;
        this.adicionadoAgenda = false;
        this.base64Image = '';
        if (this.navParams.get('evento')) {
            this.evento = this.navParams.get('evento');
            this.verificarAgenda();
            this.buscarUsuario(this.evento.idUsuario);
        }
        else {
            this.fechar();
        }
    }
    EventoPage.prototype.verificarAgenda = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.db().collection('agenda').
                            where('idUsuario', '==', this.usuario.get().uid).
                            where('idEvento', '==', this.evento.id).get().then(function (result) {
                            if (result.docs.length == 0) {
                                _this.adicionadoAgenda = false;
                            }
                            else {
                                _this.adicionadoAgenda = true;
                            }
                        }).catch(function (erro) { })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventoPage.prototype.buscarUsuario = function (idUsuario) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.db().collection('usuarios').where('uid', '==', idUsuario).get().then(function (result) {
                            if (result.docs.length > 0) {
                                _this.usuarioPub = result.docs[0].data();
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventoPage');
    };
    EventoPage.prototype.fechar = function () {
        this.view.dismiss();
    };
    EventoPage.prototype.abrirPerfil = function () {
        if (this.podeAbrirPefil) {
            this.modal.create('PerfilPage', {
                usuario: this.usuarioPub
            }).present();
        }
    };
    EventoPage.prototype.adicionarAgenda = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.db().collection('agenda').
                            where('idUsuario', '==', this.usuario.get().uid).
                            where('idEvento', '==', this.evento.id).get().then(function (result) {
                            if (result.docs.length == 0) {
                                _this.firebase.db().collection('agenda').add({
                                    idUsuario: _this.usuario.get().uid,
                                    idEvento: _this.evento.id
                                }).then(function () {
                                    _this.notificacao.adicionarMensagemSucesso('Evento adicionado a agenda com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                    _this.adicionadoAgenda = true;
                                    _this.adicionarAgendaCelular();
                                }).catch(function (erro) {
                                    _this.notificacao.adicionarMensagemErro('Não foi possível adicionar o evento a sua agenda!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                });
                            }
                            else {
                                _this.notificacao.adicionarMensagemSucesso('Evento adicionado a agenda com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                _this.adicionadoAgenda = true;
                                _this.adicionarAgendaCelular();
                            }
                        }).catch(function (erro) {
                            _this.notificacao.adicionarMensagemErro('Não foi possível adicionar o evento a sua agenda!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventoPage.prototype.adicionarAgendaCelular = function () {
        var _this = this;
        this.alertCtrl.create({
            title: 'Deseja adicionar o evento a agenda do seu dispositivo?',
            buttons: [
                {
                    text: 'Não',
                    role: 'nao',
                    handler: function (data) {
                        console.log('No clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function (data) {
                        var dataInicial = new Date(_this.evento.data);
                        var dataFinal = new Date(_this.evento.data);
                        _this.calendar.createEvent(_this.evento.nome, _this.evento.endereco.cidade + ' - ' + _this.evento.endereco.estado + '\n' + _this.evento.endereco.rua + ' ' + _this.evento.endereco.numero + '\n' + _this.evento.endereco.bairro + ',' + _this.evento.endereco.cep + '\n' + _this.evento.endereco.referencia, _this.evento.descricao, dataInicial, dataFinal).then(function () {
                            _this.notificacao.adicionarMensagemSucesso('Evento adicionado a agenda do dispositivo com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        }).catch(function (erro) {
                            console.log(erro);
                            _this.notificacao.adicionarMensagemErro('Não foi possível adicionar o evento a agenda do seu dispositivo!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        });
                    }
                }
            ]
        }).present();
    };
    EventoPage.prototype.removerAgenda = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.db().collection('agenda').
                            where('idUsuario', '==', this.usuario.get().uid).
                            where('idEvento', '==', this.evento.id).get().then(function (result) {
                            if (result.docs.length > 0) {
                                _this.firebase.db().collection('agenda').doc(result.docs[0].id).delete().then(function () {
                                    _this.notificacao.adicionarMensagemSucesso('Evento removido da agenda com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                    _this.adicionadoAgenda = false;
                                    _this.removerAgendaCelular();
                                }).catch(function (erro) {
                                    _this.notificacao.adicionarMensagemErro('Não foi possível remover o evento a sua agenda!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                });
                            }
                            else {
                                _this.notificacao.adicionarMensagemSucesso('Evento removido da agenda com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                                _this.adicionadoAgenda = false;
                                _this.removerAgendaCelular();
                            }
                        }).catch(function (erro) {
                            _this.notificacao.adicionarMensagemErro('Não foi possível remover o evento da sua agenda!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventoPage.prototype.removerAgendaCelular = function () {
        var _this = this;
        this.alertCtrl.create({
            title: 'Deseja remover o evento da agenda do seu dispositivo?',
            buttons: [
                {
                    text: 'Não',
                    role: 'nao',
                    handler: function (data) {
                        console.log('No clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: function (data) {
                        var dataInicial = new Date(_this.evento.data);
                        var dataFinal = new Date(_this.evento.data);
                        _this.calendar.deleteEvent(_this.evento.nome, _this.evento.endereco.cidade + ' - ' + _this.evento.endereco.estado + '\n' + _this.evento.endereco.rua + ' ' + _this.evento.endereco.numero + '\n' + _this.evento.endereco.bairro + ',' + _this.evento.endereco.cep + '\n' + _this.evento.endereco.referencia, _this.evento.descricao, dataInicial, dataFinal).then(function () {
                            _this.notificacao.adicionarMensagemSucesso('Evento removido da agenda do dispositivo com sucesso!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        }).catch(function (erro) {
                            console.log(erro);
                            _this.notificacao.adicionarMensagemErro('Não foi possível remover o evento da agenda do seu dispositivo!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                        });
                    }
                }
            ]
        }).present();
    };
    EventoPage.prototype.ligarParaContato = function () {
        var _this = this;
        this.callNumber.callNumber(this.evento.telefoneContato, true).then(function () { })
            .catch(function (erro) {
            _this.notificacao.adicionarMensagemErro('Não foi possível possível realizar a chamada!', _this.notificacao.BOTTOM).mostrarNotificacoes();
        });
    };
    EventoPage.prototype.compartilhar = function (appName, app) {
        var _this = this;
        var mensagem = "Nome do Evento: " + this.evento.nome + "\n\nDescri\u00E7\u00E3o do Evento: " + this.evento.descricao + "\n\nData: " + this.evento.data + "\nHor\u00E1rio: " + this.evento.horario + "\nLocal:\n\t" + this.evento.endereco.cidade + "/" + this.evento.endereco.estado + "\n\t" + this.evento.endereco.rua + " " + this.evento.endereco.numero + "\n\t" + this.evento.endereco.bairro + ", " + this.evento.endereco.cep + "\n\nOrganiza\u00E7\u00E3o\n\nNome: " + this.evento.nomeContato + "\nEmail: " + this.evento.emailContato + "\nTelefone: " + this.evento.telefoneContato;
        this.socialSharing.canShareVia(appName, mensagem, null, null).then(function () {
            _this.socialSharing.shareVia(appName, mensagem, null, null).then(function () {
            }).catch(function (erro) {
                _this.notificacao.adicionarMensagemErro('Não foi possível compartilhar o evento via ' + app + '!', _this.notificacao.BOTTOM).mostrarNotificacoes();
            });
        }).catch(function (erro) {
            console.log(erro);
            _this.notificacao.adicionarMensagemErro('Não é possível compartilhar o evento via ' + app + '!', _this.notificacao.BOTTOM).mostrarNotificacoes();
        });
    };
    //Abre conversa de chat no WhatsApp
    EventoPage.prototype.conversarWhatsapp = function () {
        var url = 'https://api.whatsapp.com/send?phone=55' + this.evento.telefoneContato;
        location.href = url;
    };
    EventoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-evento',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/evento/evento.html"*/'<!--\n\n  Generated template for the EventoPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n<ion-content>\n\n  <ion-card *ngIf="evento">\n\n    <ion-item class="with-padding" *ngIf="usuarioPub">\n\n      <ion-avatar item-start (click)="abrirPerfil()">\n\n        <img src="{{ usuarioPub.fotoPerfil }}">\n\n      </ion-avatar>\n\n      <h2 (click)="abrirPerfil()">{{ usuarioPub.nome }}</h2>\n\n      <ion-note (click)="abrirPerfil()">\n\n        Publicado em {{ evento.dataPublicacao | date:\'dd/MM/yyyy\' }}\n\n      </ion-note>\n\n      <button class="close-modal" ion-button item-end clear (click)="fechar()">\n\n        <ion-icon name="close" color="dark-gray"></ion-icon>\n\n      </button>\n\n    </ion-item>\n\n    <img src="{{ evento.foto }}">\n\n    <ion-card-content>\n\n      <div class="info">\n\n        <h1 class="titulo">{{ evento.nome }}</h1>\n\n        <p class="desc">{{ evento.descricao }}</p>\n\n        <p>Data: {{ evento.data | date:\'dd/MM/yyyy\' }}</p>\n\n        <p>Horário: {{ evento.horario }}</p>\n\n        <p>Local:\n\n          <br />\n\n          <br />{{ evento.endereco.cidade }}/{{ evento.endereco.estado }}\n\n          <br />{{ evento.endereco.rua }} {{ evento.endereco.numero }}\n\n          <br />{{ evento.endereco.bairro }}, {{ evento.endereco.cep }}\n\n          <br />{{ evento.endereco.referencia }}</p>\n\n\n\n      </div>\n\n      <div class="info">\n\n        <h1>Organização</h1>\n\n        <p>Nome: {{ evento.nomeContato }}</p>\n\n        <p>Email: {{ evento.emailContato }}</p>\n\n        <p>Telefone: {{ evento.telefoneContato }}</p>\n\n      </div>\n\n      <div class="info" *ngIf="evento.informacoesAdicionais.length > 0">\n\n        <h1>Informações Adicionais</h1>\n\n        <p>{{ evento.informacoesAdicionais }}</p>\n\n      </div>\n\n      <div class="opcoes">\n\n        <button ion-button icon-left block clear *ngIf="!adicionadoAgenda" (click)="adicionarAgenda()">\n\n          <ion-icon name="calendar"></ion-icon>\n\n          <div>Adicionar a agenda</div>\n\n        </button>\n\n        <button ion-button icon-left block clear *ngIf="adicionadoAgenda" (click)="removerAgenda()">\n\n          <ion-icon name="calendar"></ion-icon>\n\n          <div>Remover da agenda</div>\n\n        </button>\n\n        <button ion-button icon-left block clear (click)="ligarParaContato()">\n\n          <ion-icon name="call"></ion-icon>\n\n          <div>Ligar para {{ evento.nomeContato }}</div>\n\n        </button>\n\n        <button ion-button icon-left block clear (click)="conversarWhatsapp()">\n\n          <ion-icon name="logo-whatsapp"></ion-icon>\n\n          <a>Contato via WhatsApp</a>\n\n        </button>\n\n      </div>\n\n    </ion-card-content>\n\n  </ion-card>\n\n  <div *ngIf="map" id="map_canvas"></div>\n\n  <ion-fab bottom right class="fab-image">\n\n    <button ion-fab mini color="light" (click)="compartilhar(\'whatsapp\', \'WhatsApp\')">\n\n      <ion-icon color="whats" name="logo-whatsapp"></ion-icon>\n\n    </button>\n\n  </ion-fab>\n\n\n\n    <!--ion-fab-list side="top">\n\n      <button ion-fab mini (click)="compartilhar(\'whatsapp\', \'WhatsApp\')">\n\n        <ion-icon name="logo-whatsapp"></ion-icon>\n\n        <ion-label>Compartilhar</ion-label>\n\n      </button>\n\n    </ion-fab-list>\n\n  </ion-fab> -->\n\n  \n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/evento/evento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_calendar__["a" /* Calendar */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_4__providers_notificacao_notificacao__["a" /* NotificacaoProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_firebase_firebase__["a" /* FirebaseProvider */]])
    ], EventoPage);
    return EventoPage;
}());

//# sourceMappingURL=evento.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_notificacao_notificacao__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var MyApp = /** @class */ (function () {
    function MyApp(modal, platform, statusBar, splashScreen, usuario, firebase, notificacao) {
        var _this = this;
        this.modal = modal;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.usuario = usuario;
        this.firebase = firebase;
        this.notificacao = notificacao;
        this.rootPage = 'LoginPage';
        this.paginaUser = [];
        this.paginaGer = [];
        this.paginaAdm = [];
        this.solicitacoes = 0;
        this.contarSolicitacoes();
        platform.ready().then(function () {
            _this.firebase.auth().onAuthStateChanged(function (user) {
                if (!user) {
                    _this.rootPage = 'LoginPage';
                }
                else {
                    _this.rootPage = 'HomePage';
                }
                _this.hideSplashScreen();
            });
        });
        this.paginaUser = [
            { titulo: 'Home', pagina: 'HomePage', icone: '', view: true },
            { titulo: 'Meus Dados', pagina: 'MeusDadosPage', icone: '', view: true },
            { titulo: 'Meu Perfil', pagina: 'PerfilPage', icone: '', view: true }
        ];
        this.paginaGer = [
            { titulo: 'Meus Eventos', pagina: 'MeusEventosPage', icone: '', view: this.usuario.get().gerenciador },
        ];
        this.paginaAdm = [
            { titulo: 'Igrejas', pagina: 'IgrejasPage', icone: '', view: this.usuario.get().administrador },
            { titulo: 'Relatório', pagina: 'RelatoriosPage', icone: '', view: this.usuario.get().administrador },
            { titulo: 'Solicitações', pagina: 'SolicitacoesPage', icone: '', view: this.usuario.get().administrador },
            { titulo: 'Usuários', pagina: 'UsuariosPage', icone: '', view: this.usuario.get().administrador }
        ];
    }
    MyApp.prototype.hideSplashScreen = function () {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
    };
    MyApp.prototype.contarSolicitacoes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.firebase.db().collection('solicitacao').where('analise', "==", false)
                            .onSnapshot(function (snapshot) {
                            snapshot.docs.forEach(function (doc) {
                                _this.solicitacoes++;
                            });
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.abrirPagina = function (pagina) {
        if (pagina == 'PerfilPage') {
            this.modal.create(pagina, {
                usuario: this.usuario.get()
            }).present();
        }
        else if (pagina == 'SobrePage') {
            this.modal.create(pagina).present();
        }
        else {
            this.menuNav.setRoot(pagina);
        }
    };
    MyApp.prototype.sair = function () {
        this.firebase.auth().signOut();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('menuNav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */])
    ], MyApp.prototype, "menuNav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/app/app.html"*/'<ion-menu [content]="content">\n\n  <ion-content  *ngIf="rootPage != \'LoginPage\'">\n\n    <div class="menu-perfil">\n\n      <div class="foto">\n\n        <img [src]="usuario.get().fotoPerfil">\n\n      </div>\n\n      <div class="info">\n\n        <h1 class="menu-nome">{{usuario.get().nome}}</h1>\n\n        <h2 class="menu-email">{{usuario.get().email}}</h2>\n\n      </div>\n\n    </div>\n\n\n\n    <div class="menu-paginas">\n\n      <ng-container *ngFor="let pagina of paginaUser">\n\n        <button ion-item menuClose  (click)="abrirPagina(pagina.pagina)">\n\n          {{ pagina.titulo }}\n\n        </button>\n\n      </ng-container>\n\n      <ng-container *ngFor="let pagina of paginaGer">\n\n          <button ion-item menuClose  *ngIf="usuario.get().gerenciador==true" (click)="abrirPagina(pagina.pagina)">\n\n            {{ pagina.titulo }}\n\n          </button>\n\n        </ng-container>\n\n        <ng-container *ngFor="let pagina of paginaAdm">\n\n            <button class="btn-menu" ion-item menuClose  *ngIf="usuario.get().administrador==true" (click)="abrirPagina(pagina.pagina)">\n\n              {{ pagina.titulo }}\n\n              <span class="aviso" *ngIf="pagina.titulo == \'Solicitações\' && solicitacoes > 0">{{solicitacoes}}</span>\n\n            </button>\n\n          </ng-container>\n\n      <button ion-item (click)="sair()" menuClose>\n\n        Sair\n\n      </button>\n\n    </div>\n\n  </ion-content>\n\n  <ion-content *ngIf="rootPage == \'LoginPage\'">\n\n    <div class="menu-paginas">\n\n      <button ion-item menuClose (click)="abrirPagina(\'LoginPage\')">\n\n        <ion-icon [name]="user"></ion-icon>\n\n        Login\n\n      </button>\n\n      <button ion-item menuClose (click)="abrirPagina(\'CriarContaPage\')">\n\n        <ion-icon [name]="create"></ion-icon>\n\n        Cadastrar-se\n\n      </button>\n\n      <button ion-item menuClose (click)="abrirPagina(\'RedefinirSenhaPage\')">\n\n        <ion-icon [name]="create"></ion-icon>\n\n        Redefinir Senha\n\n      </button>\n\n    </div>\n\n  </ion-content>\n\n\n\n  <ion-footer>\n\n    <div class="menu-sobre">\n\n      <ion-item (click)="abrirPagina(\'SobrePage\')" menuClose>\n\n        Sobre\n\n      </ion-item>\n\n    </div>\n\n  </ion-footer>\n\n\n\n</ion-menu>\n\n\n\n<ion-nav #menuNav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_firebase_firebase__["a" /* FirebaseProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_notificacao_notificacao__["a" /* NotificacaoProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[222]);
//# sourceMappingURL=main.js.map