webpackJsonp([7],{

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CriarEventoPageModule", function() { return CriarEventoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__criar_evento__ = __webpack_require__(329);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CriarEventoPageModule = /** @class */ (function () {
    function CriarEventoPageModule() {
    }
    CriarEventoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__criar_evento__["a" /* CriarEventoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__criar_evento__["a" /* CriarEventoPage */]),
            ],
        })
    ], CriarEventoPageModule);
    return CriarEventoPageModule;
}());

//# sourceMappingURL=criar-evento.module.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CriarEventoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_native_geocoder__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(109);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the CriarEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CriarEventoPage = /** @class */ (function () {
    function CriarEventoPage(geocoder, notificacao, firebase, camera, navCtrl, navParams, view, usuario, http) {
        this.geocoder = geocoder;
        this.notificacao = notificacao;
        this.firebase = firebase;
        this.camera = camera;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.usuario = usuario;
        this.http = http;
        this.estados = [];
        this.cidades = [];
        this.dados = {
            foto: 'assets/imgs/default-banner.jpg',
            nome: '',
            descricao: '',
            data: new Date().toJSON().split('T')[0],
            horario: '',
            nomeContato: '',
            emailContato: '',
            telefoneContato: '',
            informacoesAdicionais: '',
            endereco: {
                estado: '',
                idEstado: '41',
                cidade: '',
                idCidade: '4119905',
                bairro: '',
                rua: '',
                numero: '',
                cep: '',
                referencia: ''
            },
            coordenadas: {
                latitude: '',
                longitude: ''
            }
        };
        this.foto = '';
        this.carregarEstados(false);
        this.carregarCidades(false);
        this.obterDadosCidade();
    }
    CriarEventoPage.prototype.alterarImagem = function (tipo) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 800,
            targetHeight: 800,
            correctOrientation: true
        };
        if (tipo == 'camera') {
            options.sourceType = this.camera.PictureSourceType.CAMERA;
        }
        else {
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        }
        this.camera.getPicture(options).then(function (imageData) {
            _this.foto = imageData;
            _this.dados.foto = 'data:image/jpeg;base64,' + imageData;
        }, function (err) { });
    };
    CriarEventoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditarEventoPage');
    };
    CriarEventoPage.prototype.cancelar = function () {
        this.dados = {
            foto: 'assets/imgs/default-banner.jpg',
            nome: '',
            descricao: '',
            data: new Date().toJSON().split('T')[0],
            horario: '',
            nomeContato: '',
            emailContato: '',
            telefoneContato: '',
            informacoesAdicionais: '',
            endereco: {
                estado: '',
                idEstado: '41',
                cidade: '',
                idCidade: '4119905',
                bairro: '',
                rua: '',
                numero: '',
                cep: '',
                referencia: ''
            },
            coordenadas: {
                latitude: '',
                longitude: ''
            }
        };
        this.foto = '';
    };
    CriarEventoPage.prototype.obterCoordenadas = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var endereco;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        endereco = this.dados.endereco.rua + ' ' + this.dados.endereco.numero + ' '
                            + this.dados.endereco.cidade + ' ' + this.dados.endereco.estado + ' '
                            + this.dados.endereco.cep;
                        return [4 /*yield*/, this.geocoder.forwardGeocode(endereco)
                                .then(function (coordinates) {
                                _this.dados.coordenadas.latitude = coordinates[0].latitude;
                                _this.dados.coordenadas.longitude = coordinates[0].longitude;
                            }).catch(function (erro) {
                                _this.notificacao.adicionarMensagemErro('Não foi possível salvar as coordenadas do evento.' +
                                    'O evento não será exibido no mapa!', _this.notificacao.BOTTOM);
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CriarEventoPage.prototype.salvar = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var storageRef;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        storageRef = this.firebase.storage().ref('eventos/default-banner.jpg');
                        return [4 /*yield*/, storageRef.getDownloadURL().then(function (url) {
                                _this.dados.foto = url;
                            })];
                    case 1:
                        _a.sent();
                        this.notificacao.mostrarMensagemAguarde('Aguarde a confirmação ...', this.notificacao.BOTTOM);
                        return [4 /*yield*/, this.obterCoordenadas()];
                    case 2:
                        _a.sent();
                        //Salva dados do evento
                        return [4 /*yield*/, this.firebase.db().collection('eventos').add(__assign({ idUsuario: this.usuario.get().uid }, this.dados, { dataPublicacao: new Date().toJSON().split('T')[0], dataAlteracao: new Date().toJSON().split('T')[0] })).then(function (doc) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                var storageRef_1;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!(this.foto.length > 0)) return [3 /*break*/, 2];
                                            storageRef_1 = this.firebase.storage().ref('eventos/' + doc.id + '.jpg');
                                            return [4 /*yield*/, storageRef_1.putString(this.foto, 'base64', { contentType: 'image/jpeg' }).then(function () { return __awaiter(_this, void 0, void 0, function () {
                                                    var _this = this;
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0: return [4 /*yield*/, storageRef_1.getDownloadURL().then(function (url) { return __awaiter(_this, void 0, void 0, function () {
                                                                    var _this = this;
                                                                    return __generator(this, function (_a) {
                                                                        switch (_a.label) {
                                                                            case 0:
                                                                                this.dados.foto = url;
                                                                                return [4 /*yield*/, this.firebase.db().collection('eventos').doc(doc.id).update({
                                                                                        foto: this.dados.foto
                                                                                    }).then(function () { }).catch(function (error) {
                                                                                        _this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', _this.notificacao.BOTTOM);
                                                                                        _this.dados.foto = 'assets/imgs/default-banner.jpg';
                                                                                        _this.foto = '';
                                                                                    })];
                                                                            case 1:
                                                                                _a.sent();
                                                                                return [2 /*return*/];
                                                                        }
                                                                    });
                                                                }); }).catch(function (erro) {
                                                                    _this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', _this.notificacao.BOTTOM);
                                                                    _this.dados.foto = 'assets/imgs/default-banner.jpg';
                                                                    _this.foto = '';
                                                                })];
                                                            case 1:
                                                                _a.sent();
                                                                return [2 /*return*/];
                                                        }
                                                    });
                                                }); }).catch(function (erro) {
                                                    _this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', _this.notificacao.BOTTOM);
                                                    _this.dados.foto = 'assets/imgs/default-banner.jpg';
                                                    _this.foto = '';
                                                })];
                                        case 1:
                                            _a.sent();
                                            _a.label = 2;
                                        case 2:
                                            this.notificacao.adicionarMensagemSucesso('Evento criado com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
                                            this.fechar();
                                            return [2 /*return*/];
                                    }
                                });
                            }); }).catch(function (error) {
                                _this.notificacao.adicionarMensagemErro('Erro ao criar evento!', _this.notificacao.BOTTOM).mostrarNotificacoes();
                            })];
                    case 3:
                        //Salva dados do evento
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CriarEventoPage.prototype.fechar = function () {
        this.view.dismiss();
    };
    //Carrega lista de estados para o campo estado (select)
    CriarEventoPage.prototype.carregarEstados = function (selectDefault) {
        var _this = this;
        var url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados';
        this.http.get(url).subscribe(function (data) {
            var result = data.json();
            _this.estados = [];
            if (selectDefault) {
                _this.dados.endereco.cidade = result[0].id;
            }
            result.forEach(function (estado) {
                _this.estados.push({
                    id: estado.id,
                    nome: estado.nome,
                    sigla: estado.sigla
                });
            });
        });
    };
    //Carrega lista de cidades para o campo cidade (select) de acordo com o estado selecionado
    CriarEventoPage.prototype.carregarCidades = function (selectDefault) {
        var _this = this;
        var url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/" + this.dados.endereco.idEstado + "/municipios";
        this.http.get(url).subscribe(function (data) {
            var result = data.json();
            _this.cidades = [];
            if (selectDefault) {
                _this.dados.endereco.cidade = result[0].id;
            }
            result.forEach(function (cidade) {
                _this.cidades.push({
                    id: cidade.id,
                    nome: cidade.nome
                });
            });
        });
    };
    //Obtem informações da cidade selecionada (nome da cidade e sigla do estado)
    CriarEventoPage.prototype.obterDadosCidade = function () {
        var _this = this;
        var url = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios/" + this.dados.endereco.idCidade;
        this.http.get(url).subscribe(function (data) {
            var result = data.json();
            _this.dados.endereco.estado = result.microrregiao.mesorregiao.UF.sigla;
            _this.dados.endereco.cidade = result.nome;
        });
    };
    CriarEventoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-criar-evento',template:/*ion-inline-start:"/home/marcos/Documentos/IEQspace/src/pages/criar-evento/criar-evento.html"*/'<!--\n\n  Generated template for the CriarEventoPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar color="dark">\n\n    <ion-buttons left>\n\n      <button ion-button class="icon-large">\n\n        <ion-icon name="create"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Criar Evento</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button class="icon-large" (click)="fechar()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="image">\n\n    <ion-fab top right>\n\n      <button ion-fab color="light" mini>\n\n        <ion-icon name="create"></ion-icon>\n\n      </button>\n\n      <ion-fab-list side="bottom">\n\n        <button ion-fab mini (click)="alterarImagem(\'arquivo\')">\n\n          <ion-icon name="image"></ion-icon>\n\n        </button>\n\n        <button ion-fab mini (click)="alterarImagem(\'camera\')">\n\n          <ion-icon name="camera"></ion-icon>\n\n        </button>\n\n      </ion-fab-list>\n\n    </ion-fab>\n\n    <img [src]="dados.foto">\n\n  </div>\n\n  <div class="form">\n\n    <ion-item>\n\n      <ion-label floating>Nome</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.nome"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Descrição</ion-label>\n\n      <ion-textarea type="text" rows="4" [(ngModel)]="dados.descricao"></ion-textarea>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Data</ion-label>\n\n      <ion-input type="date" [(ngModel)]="dados.data"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Horário</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.horario"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-top>\n\n      <ion-label floating>Estado</ion-label>\n\n      <ion-select [(ngModel)]="dados.endereco.idEstado" (ionChange)="carregarCidades(true)">\n\n        <ion-option *ngFor="let estado of estados, let i = index" [value]="estado.id" [selected]="this.dados.endereco.estado == estado.id" (click)="salvarEstado(estado)">{{ estado.nome }}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Cidade</ion-label>\n\n      <ion-select [(ngModel)]="dados.endereco.idCidade" (ionChange)="obterDadosCidade()">\n\n        <ion-option *ngFor="let cidade of cidades, let i = index" [value]="cidade.id" [selected]="this.dados.endereco.cidade == cidade.id" (click)="salvarCidade(cidade)">{{ cidade.nome }}</ion-option>\n\n      </ion-select>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Bairro</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.bairro"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Rua</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.rua"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Número</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.numero"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>CEP</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.cep"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-bottom>\n\n      <ion-label floating>Referência</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.endereco.referencia"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-top>\n\n      <ion-label floating>Nome do Contato para Informações</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.nomeContato"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Email para Contato</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.emailContato"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-bottom>\n\n      <ion-label floating>Telefone/Whatsapp para Contato</ion-label>\n\n      <ion-input type="text" [(ngModel)]="dados.telefoneContato"></ion-input>\n\n    </ion-item>\n\n    <ion-item padding-top>\n\n      <ion-label floating>Informações Adicionais</ion-label>\n\n      <ion-textarea type="text" rows="4" [(ngModel)]="dados.informacoesAdicionais"></ion-textarea>\n\n    </ion-item>\n\n  </div>\n\n  <div class="botoes">\n\n    <button ion-button full round (click)="salvar()">Salvar</button>\n\n    <button clear full ion-button (click)="cancelar()">Cancelar</button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/home/marcos/Documentos/IEQspace/src/pages/criar-evento/criar-evento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__["a" /* NotificacaoProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_firebase_firebase__["a" /* FirebaseProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */], __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Http */]])
    ], CriarEventoPage);
    return CriarEventoPage;
}());

//# sourceMappingURL=criar-evento.js.map

/***/ })

});
//# sourceMappingURL=7.js.map