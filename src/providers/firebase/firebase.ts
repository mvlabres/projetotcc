import { Injectable } from '@angular/core';

import * as firebase from "firebase";
import 'firebase/firestore';

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {

  constructor() {
    let config = {
      apiKey: "AIzaSyAkumw4cF2V8HI6GFBRm9v8aHI-qU6qcA4",
      authDomain: "app-crj.firebaseapp.com",
      databaseURL: "https://app-crj.firebaseio.com",
      projectId: "app-crj",
      storageBucket: "app-crj.appspot.com",
      messagingSenderId: "1059914658044"
    };
    firebase.initializeApp(config);
  }

  db(){
    return firebase.firestore();
  }

  storage(){
    return firebase.storage();
  }

  auth(){
    return firebase.auth();
  }

}
