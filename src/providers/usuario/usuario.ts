import { Injectable } from '@angular/core';

import { FirebaseProvider } from './../firebase/firebase';

/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  public usuario = {
    uid: '',
    email: '',
    nome: '',
    telefone: '',
    fotoPerfil: '',
    fotoCapa: '',
    bio: '',
    administrador:'',
    gerenciador: ''
  };

  constructor(public firebase: FirebaseProvider) {
    this.firebase.auth().onAuthStateChanged(user => {

      if (user) {

        this.usuario.uid = user.uid;
        this.usuario.email = user.email;

        //Carrega dados do usuário logado
        this.firebase.db().collection('usuarios').where('uid', '==', this.usuario.uid).onSnapshot((snapshot) => {
          snapshot.docs.forEach(doc => {

            this.usuario.nome = doc.data().nome;
            this.usuario.telefone = doc.data().telefone;
            this.usuario.fotoPerfil = doc.data().fotoPerfil;
            this.usuario.fotoCapa = doc.data().fotoCapa;
            this.usuario.bio = doc.data().bio;
            this.usuario.administrador = doc.data().administrador;
            this.usuario.gerenciador = doc.data().gerenciador;

          });
        });

      }

    });
  }

  get() {
    return this.usuario;
  }

}
