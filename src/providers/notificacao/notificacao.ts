import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';

/*
  Generated class for the NotificacaoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificacaoProvider {

  public TOP = 'top';
  public MIDDLE = 'middle';
  public BOTTOM = 'bottom';
  private notificacoes = [];

  constructor(public toast: ToastController, public loadingCtrl: LoadingController) {
    //
  }

  //Adiciona as notificações na fila (usada dentro dos metodos adicionarMensagem...)
  adicionarNotificacao(notificacao){
    this.notificacoes.push(notificacao);
    notificacao.onDidDismiss(() => {
      this.notificacoes.shift();
      if (this.notificacoes.length > 0) {
        let next = this.notificacoes[0];
        next.present();
      }
    });

    return this;
  }

  //Inicia execução da fila de mensagens
  mostrarNotificacoes() {
    if (this.notificacoes.length > 0) {
      let next = this.notificacoes[0];
      next.present();
    }
  }

  //Mostra mensagem de espera ("Aguarde Confirmação ...")
  mostrarMensagemAguarde(mensagem, posicao) {
    this.toast.create({
      message: mensagem,
      duration: 2000,
      position: posicao,
    }).present();
  }

  //Adiciona mensagem padrão na fila de mensagens
  adicionarMensagem(mensagem, posicao): NotificacaoProvider {
    this.adicionarNotificacao(this.toast.create({
      message: mensagem,
      duration: 2000,
      position: posicao,
    }));
    return this;
  }

  //Adiciona mensagem de sucesso na fila de mensagens
  adicionarMensagemSucesso(mensagem, posicao): NotificacaoProvider {
    this.adicionarNotificacao(this.toast.create({
      message: mensagem,
      duration: 2000,
      position: posicao,
      cssClass: 'toast-sucesso',
    }));
    return this;
  }

  //Adiciona mensagem de erro na fila de mensagens
  adicionarMensagemErro(mensagem, posicao): NotificacaoProvider {
    this.adicionarNotificacao(this.toast.create({
      message: mensagem,
      duration: 2000,
      position: posicao,
      cssClass: 'toast-erro',
    }));
    return this;
  }

  //Adiciona mensagem de aviso na fila de mensagens
  adicionarMensagemAviso(mensagem, posicao): NotificacaoProvider {
    this.adicionarNotificacao(this.toast.create({
      message: mensagem,
      duration: 2000,
      position: posicao,
      cssClass: 'toast-aviso',
    }));
    return this;
  }

}
