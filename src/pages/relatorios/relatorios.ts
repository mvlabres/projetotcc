import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the RelatoriosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-relatorios',
  templateUrl: 'relatorios.html',
})
export class RelatoriosPage {
  public countIgrejas = 0;
  public countUsuarios = 0;
  public countEventos = 0;
  public countGerenciadores = 0;
  public countSolicitacoes = 0;
  public countUsusarioComum = 0;

  constructor(public firebase: FirebaseProvider, public view: ViewController,public navCtrl: NavController, public navParams: NavParams) {
    this.contarIgrejas();
    this.contarUsuarios();
    this.contarEventos();
    this.contarGerenciadores();
    this.contarsolicitacoes();
    this.countUsusarioComum = this.countUsuarios - this.countGerenciadores;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RelatoriosPage');
  }

  async contarIgrejas(){
    this.firebase.db().collection("igreja").onSnapshot((snapshot)=>{
      snapshot.docs.forEach(doc => {
        this.countIgrejas ++;
      })
    })
  }

  async contarUsuarios(){
    this.firebase.db().collection("usuarios").onSnapshot((snapshot)=>{
      snapshot.docs.forEach(doc => {
        this.countUsuarios ++;
      })
    })
  }

  async contarEventos(){
    this.firebase.db().collection("eventos").onSnapshot((snapshot)=>{
      snapshot.docs.forEach(doc => {
        this.countEventos ++;
      })
    })
  }

  async contarGerenciadores(){
    this.firebase.db().collection("usuarios").where("gerenciador","==",true).onSnapshot((snapshot)=>{
      snapshot.docs.forEach(doc => {
        this.countGerenciadores ++;
      })
    })
  }

  async contarsolicitacoes(){
    this.firebase.db().collection("solicitacao").onSnapshot((snapshot)=>{
      snapshot.docs.forEach(doc => {
        this.countSolicitacoes ++;
      })
    })
  }
}
