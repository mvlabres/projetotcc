import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';


/**
 * Generated class for the IgrejasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-igrejas',
  templateUrl: 'igrejas.html',
})
export class IgrejasPage {

  public igrejas = [];
  public membros = 0;
  public countIgrejas = 0;
  public novaIgreja = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public modal: ModalController, public usuario: UsuarioProvider, public firebase: FirebaseProvider, public notificacao: NotificacaoProvider) {
    this.carregarIgrejas();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IgrejasPage');
  }

  async carregarIgrejas(){
    await this.firebase.db().collection("igreja").orderBy("nome").onSnapshot((snapshot)=>{
      
      snapshot.docs.forEach(async doc => {
        this.igrejas = [];
        await this.firebase.db().collection("usuarios").where("igreja", "==", doc.data().nome).get().then((result) => {
          this.membros = result.docs.length;
        })
        this.igrejas.push({id: doc.id, ...doc.data(), membros: this.membros})
        this.countIgrejas ++;
        this.novaIgreja = this.countIgrejas + 1;
      })
    })
  }
  async newIgreja(){
    await this.firebase.db().collection("igreja").add({
      nome: this.novaIgreja,
    })
    this.carregarIgrejas();
  }
  addIgreja(){
      let alert = this.alertCtrl.create({
        title: 'Confirme',
        message: 'Tem certeza que deseja incluir a ' + this.novaIgreja + ' IEQ?',
        buttons: [
          {
            text: 'Não',
            role: 'não',
          },
          {
            text: 'Sim',
            role: 'sim',
            handler: () => {
              this.newIgreja();
            }
          }
        ]
      });
      alert.present();
    
  }
}
