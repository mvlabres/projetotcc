import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the MinhaAgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-minha-agenda',
  templateUrl: 'minha-agenda.html',
})
export class MinhaAgendaPage {

  public eventos = [];
  public unsubscribe = () => {};
  public filtro;

  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams, public firebase: FirebaseProvider, public usuario: UsuarioProvider, public modal: ModalController) {
    this.filtro = this.navParams.data;
    this.carregarEventos();
    events.subscribe('filtrar-agenda', () => {
      this.carregarEventos();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MinhaAgendaPage');
  }

  carregarEventos() {
    this.unsubscribe();

    this.unsubscribe = this.firebase.db().collection('agenda').
    where('idUsuario', '==', this.usuario.get().uid).
    onSnapshot((snapshot) => {

        this.eventos = [];
        snapshot.docs.forEach(doc => {
          this.firebase.db().collection('eventos').doc(doc.data().idEvento).get().then((evento) => {
            
            if(evento.exists){

              if(this.filtro.dataInicial != undefined && this.filtro.dataInicial.length > 0){
                if (Date.parse(evento.data().data) < Date.parse(this.filtro.dataInicial)) {
                  return;
                }
              }
          
              if(this.filtro.dataFinal != undefined && this.filtro.dataFinal.length > 0){
                if (Date.parse(evento.data().data) > Date.parse(this.filtro.dataFinal)) {
                  return;
                }
              }
              
              this.eventos.push(
                {
                  id: evento.id,
                  ...evento.data()
                }
              );

            }
            
          });

        });
      });
  }

  abrirDetalhes(evento){
    this.modal.create("EventoPage", {
      evento: evento
    }).present();
  }

}
