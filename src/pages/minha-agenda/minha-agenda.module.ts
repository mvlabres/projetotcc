import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhaAgendaPage } from './minha-agenda';

@NgModule({
  declarations: [
    MinhaAgendaPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhaAgendaPage),
  ],
})
export class MinhaAgendaPageModule {}
