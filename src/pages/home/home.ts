import { Component, ViewChild, } from '@angular/core';
import { IonicPage, NavController, NavParams, Searchbar, AlertController, Tabs, Events } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild('searchbar') searchbar: Searchbar;
  @ViewChild('tabs') tabs: Tabs;

  public homeTab: any = 'EventosPage';
  public mapaTab: any = 'MapaPage';
  public agendaTab: any = 'MinhaAgendaPage';
  public filtro = {
    dataInicial: '',
    dataFinal: '',
    texto: ''
  }
  public solicitacoes = 0;
  public titulo = 'Home';
  public tab = 'home';

  constructor(public usuario:UsuarioProvider, public firebase:FirebaseProvider,  public events: Events, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController ) {
    this.firebase.db().collection('solicitacao').where("analise","==", false)
    .onSnapshot((snapshot)=>{
      snapshot.docs.forEach(doc => {
          this.solicitacoes ++;
      })
    })
  }

  placeholder() {
    return this.tab == 'mapa' ? 'Local ou Região' : 'Nome ou Descrição';
  }

  async contarSolicitacoes(){
   
  }

  public carregarEventos(){
    this.events.publish('filtrar-' + this.tab);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  alterarTab(tab) {
    this.tab = tab;
    switch(tab){
      case 'home':
        this.titulo = 'Home';
        break;
      case 'mapa':
        this.titulo = 'Mapa de Eventos';
        break;
      case 'agenda':
        this.titulo = 'Minha Agenda';
        break;
    }
  }

  buscarPorData() {
    let alert = this.alertCtrl.create({
      title: 'Pesquisar Por Período',
      inputs: [
        {
          name: 'dataInicial',
          placeholder: 'Data Inicial',
          type: 'date',
          value: this.filtro.dataInicial
        },
        {
          name: 'dataFinal',
          placeholder: 'Data Final',
          type: 'date',
          value: this.filtro.dataFinal
        }
      ],
      buttons: [
        {
          text: 'Limpar',
          role: 'limpar',
          handler: data => {
            this.filtro.dataInicial = '';
            this.filtro.dataFinal = '';
            this.carregarEventos();
          }
        },
        {
          text: 'Pesquisar',
          handler: data => {
            this.filtro.dataInicial = data.dataInicial;
            this.filtro.dataFinal = data.dataFinal;
            this.carregarEventos();
          }
        }
      ]
    });
    alert.present();
  }

}
