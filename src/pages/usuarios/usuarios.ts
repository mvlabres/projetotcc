import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the UsuariosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-usuarios',
  templateUrl: 'usuarios.html',
})
export class UsuariosPage {
  public usuarios = [];

  constructor(public usuario:UsuarioProvider,public firebase:FirebaseProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.carregarUsuarios();
  }

  async carregarUsuarios(){
    await this.firebase.db().collection('usuarios').orderBy('nome').onSnapshot((snapshot) => {
      this.usuarios = []
      snapshot.docs.forEach((doc) => {
        
        this.usuarios.push({id: doc.id, ...doc.data()})
      });
    });
  }

  gerenciar(uid, color){
    if(color == 'secondary'){
      
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsuariosPage');
  }

}
