import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public titulo = 'Perfil';
  public eventos = [];
  public dados;

  constructor(public view: ViewController, public navCtrl: NavController, public navParams: NavParams, public modal: ModalController, public usuario: UsuarioProvider, public firebase: FirebaseProvider, public notificacao: NotificacaoProvider) {
    if(this.navParams.get('usuario')){
      this.dados = this.navParams.get('usuario');
      if(this.dados.uid == this.usuario.get().uid){
        this.titulo = 'Meu Perfil';
      }
      this.carregarEventos(this.dados.uid);
    }else{
      this.sair();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  async carregarEventos(idUsuario) {
    this.firebase.db().collection('eventos').
    where('idUsuario', '==', idUsuario).
    orderBy('dataPublicacao', 'desc').
    onSnapshot((snapshot) => {
      this.eventos = [];
      snapshot.docs.forEach(doc => {
        this.eventos.push({ id: doc.id, ...doc.data() });
      });
    });
  }

  abrirDetalhes(evento) {
    this.modal.create("EventoPage", {
      evento: evento,
      abrirPerfil: false
    }).present();
  }

  sair(){
    this.view.dismiss();
  }

  //Abre conversa de chat no WhatsApp
  conversarWhatsapp(){
    let url = 'https://api.whatsapp.com/send?phone=55' + this.dados.telefone;
    location.href = url;
  }

}
