import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';

/**
 * Generated class for the RedefinirSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-redefinir-senha',
  templateUrl: 'redefinir-senha.html',
})
export class RedefinirSenhaPage {

  public email = '';

  constructor(public modal: ModalController, public notificacao: NotificacaoProvider, public firebase: FirebaseProvider, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RedefinirSenhaPage');
  }

  login(){
    this.navCtrl.setRoot('LoginPage');
  }

  redefinir(){
    //Envia emial de redefinição de senha //pelo firebase
    this.firebase.auth().sendPasswordResetEmail(this.email).then(() => {
      this.notificacao.adicionarMensagemSucesso('Email de redefinição de senha enviado com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
    }).catch((erro) => {
      this.notificacao.adicionarMensagemErro('Não foi possível enviar o email de redefinição de senha!', this.notificacao.BOTTOM).mostrarNotificacoes();
    });
  }

  abrirInformacao(){
    this.modal.create('SobrePage').present();
  }

}
