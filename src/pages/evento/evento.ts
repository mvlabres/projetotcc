import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';
import { Calendar } from '@ionic-native/calendar';
import { NativeGeocoder } from '@ionic-native/native-geocoder';

/**
 * Generated class for the EventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-evento',
  templateUrl: 'evento.html',
})
export class EventoPage {

  public evento;
  public usuarioPub = {};
  public podeAbrirPefil = true;
  public adicionadoAgenda = false;
  public base64Image = '';

  constructor(public geocoder: NativeGeocoder, public alertCtrl: AlertController, public calendar: Calendar, public callNumber: CallNumber, public socialSharing: SocialSharing, public notificacao: NotificacaoProvider, public modal: ModalController, public navCtrl: NavController, public navParams: NavParams, public view: ViewController, public usuario: UsuarioProvider, public firebase: FirebaseProvider) {
    if (this.navParams.get('evento')) {
      this.evento = this.navParams.get('evento');
      this.verificarAgenda();
      this.buscarUsuario(this.evento.idUsuario);
    } else {
      this.fechar();
    }
  }

  async verificarAgenda() {

    await this.firebase.db().collection('agenda').
      where('idUsuario', '==', this.usuario.get().uid).
      where('idEvento', '==', this.evento.id).get().then((result) => {
        if (result.docs.length == 0) {
          this.adicionadoAgenda = false;
        } else {
          this.adicionadoAgenda = true;
        }
      }).catch((erro) => {});

  }

  async buscarUsuario(idUsuario) {

    await this.firebase.db().collection('usuarios').where('uid', '==', idUsuario).get().then((result) => {
      if (result.docs.length > 0) {
        this.usuarioPub = result.docs[0].data();
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventoPage');
  }

  fechar() {

    this.view.dismiss();

  }

  abrirPerfil() {

    if (this.podeAbrirPefil) {
      this.modal.create('PerfilPage', {
        usuario: this.usuarioPub
      }).present();
    }

  }

  async adicionarAgenda() {

    await this.firebase.db().collection('agenda').
      where('idUsuario', '==', this.usuario.get().uid).
      where('idEvento', '==', this.evento.id).get().then((result) => {
        if (result.docs.length == 0) {
          this.firebase.db().collection('agenda').add({
            idUsuario: this.usuario.get().uid,
            idEvento: this.evento.id
          }).then(() => {
            this.notificacao.adicionarMensagemSucesso('Evento adicionado a agenda com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
            this.adicionadoAgenda = true;
            this.adicionarAgendaCelular();
          }).catch((erro) => {
            this.notificacao.adicionarMensagemErro('Não foi possível adicionar o evento a sua agenda!', this.notificacao.BOTTOM).mostrarNotificacoes();
          });
        } else {
          this.notificacao.adicionarMensagemSucesso('Evento adicionado a agenda com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
          this.adicionadoAgenda = true;
          this.adicionarAgendaCelular();
        }
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível adicionar o evento a sua agenda!', this.notificacao.BOTTOM).mostrarNotificacoes();
      });

  }

  adicionarAgendaCelular() {

    this.alertCtrl.create({
      title: 'Deseja adicionar o evento a agenda do seu dispositivo?',
      buttons: [
        {
          text: 'Não',
          role: 'nao',
          handler: data => {
            console.log('No clicked');
          }
        },
        {
          text: 'Sim',
          handler: data => {
            let dataInicial = new Date(this.evento.data);
            let dataFinal = new Date(this.evento.data);

            this.calendar.createEvent(this.evento.nome, this.evento.endereco.cidade + ' - ' + this.evento.endereco.estado + '\n' + this.evento.endereco.rua + ' ' + this.evento.endereco.numero + '\n' + this.evento.endereco.bairro + ',' + this.evento.endereco.cep + '\n' + this.evento.endereco.referencia, this.evento.descricao, dataInicial, dataFinal).then(() => {
              this.notificacao.adicionarMensagemSucesso('Evento adicionado a agenda do dispositivo com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
            }).catch((erro) => {
              console.log(erro);
              this.notificacao.adicionarMensagemErro('Não foi possível adicionar o evento a agenda do seu dispositivo!', this.notificacao.BOTTOM).mostrarNotificacoes();
            });

          }
        }
      ]
    }).present();

  }

  async removerAgenda() {

    await this.firebase.db().collection('agenda').
      where('idUsuario', '==', this.usuario.get().uid).
      where('idEvento', '==', this.evento.id).get().then((result) => {
        if (result.docs.length > 0) {

          this.firebase.db().collection('agenda').doc(result.docs[0].id).delete().then(() => {
            this.notificacao.adicionarMensagemSucesso('Evento removido da agenda com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
            this.adicionadoAgenda = false;
            this.removerAgendaCelular();
          }).catch((erro) => {
            this.notificacao.adicionarMensagemErro('Não foi possível remover o evento a sua agenda!', this.notificacao.BOTTOM).mostrarNotificacoes();
          });

        } else {
          this.notificacao.adicionarMensagemSucesso('Evento removido da agenda com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
          this.adicionadoAgenda = false;
          this.removerAgendaCelular();
        }
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível remover o evento da sua agenda!', this.notificacao.BOTTOM).mostrarNotificacoes();
      });

  }

  removerAgendaCelular() {

    this.alertCtrl.create({
      title: 'Deseja remover o evento da agenda do seu dispositivo?',
      buttons: [
        {
          text: 'Não',
          role: 'nao',
          handler: data => {
            console.log('No clicked');
          }
        },
        {
          text: 'Sim',
          handler: data => {
            let dataInicial = new Date(this.evento.data);
            let dataFinal = new Date(this.evento.data);

            this.calendar.deleteEvent(this.evento.nome, this.evento.endereco.cidade + ' - ' + this.evento.endereco.estado + '\n' + this.evento.endereco.rua + ' ' + this.evento.endereco.numero + '\n' + this.evento.endereco.bairro + ',' + this.evento.endereco.cep + '\n' + this.evento.endereco.referencia, this.evento.descricao, dataInicial, dataFinal).then(() => {
              this.notificacao.adicionarMensagemSucesso('Evento removido da agenda do dispositivo com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
            }).catch((erro) => {
              console.log(erro);
              this.notificacao.adicionarMensagemErro('Não foi possível remover o evento da agenda do seu dispositivo!', this.notificacao.BOTTOM).mostrarNotificacoes();
            });

          }
        }
      ]
    }).present();

  }

  ligarParaContato() {

    this.callNumber.callNumber(this.evento.telefoneContato, true).then(() => { })
      .catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível possível realizar a chamada!', this.notificacao.BOTTOM).mostrarNotificacoes();
      });

  }

  compartilhar(appName, app) {

    let mensagem =  `Nome do Evento: ${this.evento.nome}\n\nDescrição do Evento: ${this.evento.descricao}\n\nData: ${this.evento.data}\nHorário: ${this.evento.horario}\nLocal:\n\t${this.evento.endereco.cidade}/${this.evento.endereco.estado}\n\t${this.evento.endereco.rua } ${this.evento.endereco.numero }\n\t${this.evento.endereco.bairro}, ${this.evento.endereco.cep}\n\nOrganização\n\nNome: ${this.evento.nomeContato}\nEmail: ${this.evento.emailContato}\nTelefone: ${this.evento.telefoneContato}`;

    this.socialSharing.canShareVia(appName, mensagem, null, null).then(() => {
      this.socialSharing.shareVia(appName, mensagem, null, null).then(() => {
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível compartilhar o evento via ' + app + '!', this.notificacao.BOTTOM).mostrarNotificacoes();
      });

    }).catch((erro) => {
      console.log(erro);
      this.notificacao.adicionarMensagemErro('Não é possível compartilhar o evento via ' + app + '!', this.notificacao.BOTTOM).mostrarNotificacoes();
    });

  }

  //Abre conversa de chat no WhatsApp
  conversarWhatsapp(){
    let url = 'https://api.whatsapp.com/send?phone=55' + this.evento.telefoneContato;
    location.href = url;
  }

}
