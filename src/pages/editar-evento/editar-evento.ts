import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';
import { NativeGeocoder, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Http } from '@angular/http';

/**
 * Generated class for the EditarEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-evento',
  templateUrl: 'editar-evento.html',
})
export class EditarEventoPage {

  public estados = [];
  public cidades = [];

  public dados;
  public foto = '';

  constructor(public http: Http, public geocoder: NativeGeocoder, public notificacao: NotificacaoProvider, public firebase: FirebaseProvider, public camera: Camera, public navCtrl: NavController, public navParams: NavParams, public view: ViewController, public usuario: UsuarioProvider) {
    this.dados = this.navParams.get('evento');
    this.carregarEstados(false);
    this.carregarCidades(false);    
    this.obterDadosCidade();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarEventoPage');
  }

  cancelar() {
    this.dados = this.navParams.get('evento');
    this.foto = '';
  }

  alterarImagem(tipo) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 800,
      correctOrientation: true
    }

    if (tipo == 'camera') {
      options.sourceType = this.camera.PictureSourceType.CAMERA;
    } else {
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    }

    this.camera.getPicture(options).then((imageData) => {
      this.foto = imageData;
      this.dados.foto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => { });
  }

  async obterCoordenadas() {
    //Salva coordenados do endereço cadastro
    let endereco = this.dados.endereco.rua + ' ' + this.dados.endereco.numero + ' ' + this.dados.endereco.cidade + ' ' + this.dados.endereco.estado + ' ' + this.dados.endereco.cep;
    await this.geocoder.forwardGeocode (endereco)
      .then((coordinates: NativeGeocoderForwardResult) => {
        this.dados.coordenadas.latitude = coordinates[0].latitude;
        this.dados.coordenadas.longitude = coordinates[0].longitude;
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível salvar as coordenadas do evento. O evento não será exibido no mapa!', this.notificacao.BOTTOM);
      });
  }

  async salvar() {

    this.notificacao.mostrarMensagemAguarde('Aguarde a confirmação ...', this.notificacao.BOTTOM);

    if (this.foto.length > 0) {
      //Envia/Salva a imagem de evento inserida (camera ou arquivo do dispositivo)
      let storageRef = this.firebase.storage().ref('eventos/' + this.dados.id + '.jpg');
      await storageRef.putString(this.foto, 'base64', { contentType: 'image/jpeg' }).then(async () => {
        await storageRef.getDownloadURL().then(async (url) => {
          this.dados.foto = url;
          this.foto = '';
        }).catch((erro) => {
          this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', this.notificacao.BOTTOM);
          this.foto = '';
        });
      });

    }
    await this.obterCoordenadas();

      //Salva dados do evento
    await this.firebase.db().collection('eventos').doc(this.dados.id).update({
      ...this.dados,
      dataAlteracao: new Date().toJSON().split('T')[0]
    }).then(async (doc) => {
      this.notificacao.adicionarMensagemSucesso('Evento editado com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
      this.fechar();
    }).catch((error) => {
      this.notificacao.adicionarMensagemErro('Erro ao editar evento!', this.notificacao.BOTTOM).mostrarNotificacoes();
    });

  }

  fechar() {
    this.view.dismiss();
  }

  //Carrega lista de estados para o campo estado (select)
  carregarEstados(selectDefault) {

    let url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados';

    this.http.get(url).subscribe(data => {
      let result = data.json();
      this.estados = [];

      if(selectDefault){
        this.dados.endereco.cidade = result[0].id;
      }
      
      result.forEach((estado) => {
        this.estados.push({
          id: estado.id,
          nome: estado.nome,
          sigla: estado.sigla
        });
      });    
    });

  }

  //Carrega lista de cidades para o campo cidade (select) de acordo com o estado selecionado
  carregarCidades(selectDefault) {

    let url = `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${this.dados.endereco.idEstado}/municipios`;

    this.http.get(url).subscribe(data => {
      let result = data.json();
      this.cidades = [];

      if(selectDefault){
        this.dados.endereco.cidade = result[0].id;
      }

      result.forEach((cidade) => {
        this.cidades.push({
          id: cidade.id,
          nome: cidade.nome
        });
      });   
    });

  }

  //Obtem informações da cidade selecionada (nome da cidade e sigla do estado)
  obterDadosCidade() {

    let url = `https://servicodados.ibge.gov.br/api/v1/localidades/municipios/${this.dados.endereco.idCidade}`;

    this.http.get(url).subscribe(data => {
      let result = data.json();
      this.dados.endereco.estado = result.microrregiao.mesorregiao.UF.sigla;
      this.dados.endereco.cidade = result.nome;
    });

  }


}
