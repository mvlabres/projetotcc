import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';

/**
 * Generated class for the EventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html',
})
export class EventosPage {
  
  public eventos = [];
  public unsubscribe = () => { };
  public filtro = {
    dataInicial: '',
    dataFinal: '',
    texto: ''
  };

  constructor(public notificacao: NotificacaoProvider, public events: Events, public navCtrl: NavController, public navParams: NavParams, public firebase: FirebaseProvider, public usuario: UsuarioProvider, public modal: ModalController) {
    this.filtro = this.navParams.data;
    this.carregarEventos();
    events.subscribe('filtrar-eventos', () => {
      this.carregarEventos();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventosPage');
  }

  async carregarEventos() {
    
    this.unsubscribe();

    let ref =await this.firebase.db().collection('eventos').orderBy('data', 'desc');

    if(this.filtro.dataInicial != undefined && this.filtro.dataInicial.length > 0){
      ref = ref.where('data', '>=', this.filtro.dataInicial);
    }

    if(this.filtro.dataFinal != undefined && this.filtro.dataFinal.length > 0){
      ref = ref.where('data', '<=', this.filtro.dataFinal);
    }

    this.unsubscribe = ref.onSnapshot((snapshot) => {
      this.eventos = [];
      snapshot.docs.forEach(async doc => {

        await this.firebase.db().collection('usuarios').where('uid', '==', doc.data().idUsuario).get().then(async (result) => {
          if (result.docs.length > 0) {
            await this.eventos.push({ id: doc.id, ...doc.data(), usuario: { ...result.docs[0].data() }});            
          }else {
            await this.eventos.push({ id: doc.id, ...doc.data(), usuario: { fotoPerfil: './../../assets/imgs/default-user.jpg', nome: 'Sem Identificação' } });
          }

        }).catch((erro) => {});

      });
    });
  }

  abrirDetalhes(evento) {
    this.modal.create("EventoPage", {
      evento: evento,
      abrirPerfil: true
    }).present();
  }

  abrirPerfil(usuario) {
    this.modal.create('PerfilPage', {
      usuario: usuario
    }).present();
  }

  async  participar(icon, evento){
    if(icon == "hand"){
        await this.firebase.db().collection("participar").add({
        idUser:this.usuario.get().uid,
        idEvento:evento.id,
        data:new Date().toJSON().split('T')[0]
      });
    }
    else{
      await this.firebase.db().collection("participar").doc(evento.idParticipar).delete();
    }
    this.carregarEventos();
    
  }
}
