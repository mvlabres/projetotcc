import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';

/**
 * Generated class for the MeusEventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-meus-eventos',
  templateUrl: 'meus-eventos.html',
})
export class MeusEventosPage {

  public eventos = [];
  public dataInicial = '';
  public dataFinal = '';
  public unsubscribe = () => {};


  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public modal: ModalController, public usuario: UsuarioProvider, public firebase: FirebaseProvider, public notificacao: NotificacaoProvider) {
    this.carregarEventos();
  }

  carregarEventos() {
    
    this.unsubscribe();

    let ref = this.firebase.db().collection('eventos').
    where('idUsuario', '==', this.usuario.get().uid);

    if(this.dataInicial.length > 0){
      ref = ref.where('data', '>=', this.dataInicial);
    }

    if(this.dataFinal.length > 0){
      ref = ref.where('data', '<=', this.dataFinal);
    }

    ref = ref.orderBy('data', 'desc');

    this.unsubscribe = ref.onSnapshot((snapshot) => {
      this.eventos = [];
      snapshot.docs.forEach(doc => {
        this.eventos.push({id: doc.id, ...doc.data()});
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MeusEventosPage');
  }

  criarEvento(){
    this.modal.create("CriarEventoPage").present();
  }

  editarEvento(evento){
    this.modal.create("EditarEventoPage", {
      evento: evento
    }).present();
  }

  excluirEvento(evento) {

    let alert = this.alertCtrl.create({
      title: 'Excluir Evento',
      subTitle: 'Deseja realmente excluir o evento ' + evento.nome + ' na data ' + evento.data + '?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
          handler: data => {}
        },
        {
          text: 'Excluir',
          handler: data => {
          
            this.notificacao.mostrarMensagemAguarde('Aguarde ...', this.notificacao.BOTTOM);

            this.firebase.db().collection('eventos').doc(evento.id).delete().then(() => {

              this.firebase.db().collection('eventos').where('idEvento', '==', evento.id).get().then((result) => {
                result.docs.forEach((doc) => {
                  this.firebase.db().collection('agenda').doc(doc.id).delete();
                });
              });

              this.notificacao.adicionarMensagemSucesso('Evento excluído com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
            }).catch((erro) => {
              this.notificacao.adicionarMensagemErro('Erro ao excluir evento!', this.notificacao.BOTTOM).mostrarNotificacoes();
            });

          }
        }
      ]
    });
    alert.present();
  }

  buscarPorData() {
    let alert = this.alertCtrl.create({
      title: 'Pesquisar Por Período',
      inputs: [
        {
          name: 'dataInicial',
          placeholder: 'Data Inicial',
          type: 'date',
          value: this.dataInicial
        },
        {
          name: 'dataFinal',
          placeholder: 'Data Final',
          type: 'date',
          value: this.dataFinal
        }
      ],
      buttons: [
        {
          text: 'Limpar',
          role: 'limpar',
          handler: data => {
            this.dataInicial = '';
            this.dataFinal = '';
            this.carregarEventos();
          }
        },
        {
          text: 'Pesquisar',
          handler: data => {
            this.dataInicial = data.dataInicial;
            this.dataFinal = data.dataFinal;
            this.carregarEventos();
          }
        }
      ]
    });
    alert.present();
  }

}
