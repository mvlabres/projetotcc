import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { FirebaseProvider } from './../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public logar = {
    email: '',
    senha: ''
  };

  formGroup:FormGroup;
  email:AbstractControl;
  senha:AbstractControl;

  constructor(public modal: ModalController, public formeBuilder:FormBuilder, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public firebase: FirebaseProvider, public notificacao: NotificacaoProvider) {
    
    this.formGroup = this.formeBuilder.group({
      email:['', Validators.required],
      senha:['', Validators.required]
    });

    this.email = this.formGroup.controls['email'];
    this.senha = this.formGroup.controls['senha'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async login(){
    try{
      if( this.email.hasError('required')){
        this.notificacao.adicionarMensagemErro('E-mail é obrigatório!', this.notificacao.BOTTOM).mostrarNotificacoes();
        return;
      }
      else if(this.senha.hasError('required')){
        this.notificacao.adicionarMensagemErro('Senha é obrigatória!', this.notificacao.BOTTOM).mostrarNotificacoes();
        return;
      }
      await this.firebase.auth().signInWithEmailAndPassword(this.logar.email, this.logar.senha);
    } catch(e){
      this.notificacao.adicionarMensagemErro('E-mail ou senha incorretos!', this.notificacao.BOTTOM).mostrarNotificacoes();
    }
  }

  criarConta(){
    this.navCtrl.push('CriarContaPage');
  }

  redefinirSenha(){
    this.navCtrl.push('RedefinirSenhaPage');
  }

  abrirInformacao(){
    this.modal.create('SobrePage').present();
  }

}
