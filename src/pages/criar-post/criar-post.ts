import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CameraOptions,Camera } from '@ionic-native/camera';
import { Http } from '@angular/http';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the CriarPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-post',
  templateUrl: 'criar-post.html',
})
export class CriarPostPage {
  public dados = {
    foto: 'assets/imgs/default-banner.jpg',
    titulo:'',
    mensagem: '',
    data: new Date().toJSON().split('T')[0],
  }
  public foto = '';

  constructor(public view:ViewController, public navCtrl: NavController, public navParams: NavParams,
              public firebase:FirebaseProvider, public camera: Camera,public http: Http,public notificacao:NotificacaoProvider,
              public usuario:UsuarioProvider) {
  }

  alterarImagem(tipo) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 800,
      correctOrientation: true
    }

    if (tipo == 'camera') {
      options.sourceType = this.camera.PictureSourceType.CAMERA;
    } else {
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    }

    this.camera.getPicture(options).then((imageData) => {
      this.foto = imageData;
      this.dados.foto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => { });
  }

  async salvar() {

    //Obtem url da imagem padrão de evento
  let storageRef = this.firebase.storage().ref('eventos/default-banner.jpg');
  await storageRef.getDownloadURL().then((url) => {
    this.dados.foto = url;
  });


  //Salva dados do post
  await this.firebase.db().collection('post').add({
    idUsuario: this.usuario.get().uid,
    ...this.dados,
    dataPublicacao: new Date().toJSON().split('T')[0],
    dataAlteracao: new Date().toJSON().split('T')[0]
  }).then(async (doc) => {

    if (this.foto.length > 0) {
      //Envia/Salva a imagem oe post inserida (camera ou arquivo do dispositivo)
      let storageRef = this.firebase.storage().ref('post/' + doc.id + '.jpg');
      await storageRef.putString(this.foto, 'base64', { contentType: 'image/jpeg' }).then(async () => {
        await storageRef.getDownloadURL().then(async (url) => {
          this.dados.foto = url;
          await this.firebase.db().collection('post').doc(doc.id).update({
            foto: this.dados.foto
          }).then(() => { }).catch((error) => {
            this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do post!', this.notificacao.BOTTOM);
            this.dados.foto = 'assets/imgs/default-banner.jpg';
            this.foto = '';
          });
        }).catch((erro) => {
          this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do post!', this.notificacao.BOTTOM);
          this.dados.foto = 'assets/imgs/default-banner.jpg';
          this.foto = '';
        });
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do post!', this.notificacao.BOTTOM);
        this.dados.foto = 'assets/imgs/default-banner.jpg';
        this.foto = '';
      });

    }

    this.notificacao.adicionarMensagemSucesso('Post criado com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
    this.fechar();
  }).catch((error) => {
    this.notificacao.adicionarMensagemErro('Erro ao criar post!', this.notificacao.BOTTOM).mostrarNotificacoes();
  });


}

  fechar() {
    this.view.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CriarPostPage');
  }

}
