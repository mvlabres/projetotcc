import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the MeusDadosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-meus-dados',
  templateUrl: 'meus-dados.html',
})
export class MeusDadosPage {

  public dados;
  public acesso = {
    email: '',
    senha: '',
    novaSenha: '',
    confirmacaoNovaSenha: '',
  };
  public foto = {
    capa: '',
    perfil: '',
  };

  constructor(public alertCtrl: AlertController, public firebase: FirebaseProvider, public notificacao: NotificacaoProvider, private camera: Camera, public navCtrl: NavController, public navParams: NavParams, public usuario: UsuarioProvider) {
    this.dados = {
      ...this.usuario.get()
    };
    this.acesso.email = this.usuario.get().email;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MeusDadosPage');
  }

  alterarFotoCapa(tipo) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 800,
      correctOrientation: true
    }

    if (tipo == 'camera') {
      options.sourceType = this.camera.PictureSourceType.CAMERA;
    } else {
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    }

    this.camera.getPicture(options).then((imageData) => {
      this.foto.capa = imageData;
      this.dados.fotoCapa = 'data:image/jpeg;base64,' + imageData;
    }, (err) => { });
  }

  alterarFotoPerfil(tipo) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 800,
      correctOrientation: true
    }

    if (tipo == 'camera') {
      options.sourceType = this.camera.PictureSourceType.CAMERA;
    } else {
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    }

    this.camera.getPicture(options).then((imageData) => {
      this.foto.perfil = imageData;
      this.dados.fotoPerfil = 'data:image/jpeg;base64,' + imageData;
    }, (err) => { });
  }

  cancelar() {
    this.dados = {
      ...this.usuario.get()
    };
    this.acesso.email = this.usuario.get().email;
    this.acesso.senha = '';
    this.acesso.novaSenha = '';
    this.acesso.confirmacaoNovaSenha = '';
  }

  async salvar() {

    //Verificação de confirmação de senha
    if (this.acesso.novaSenha.length > 0) {
      if (this.acesso.novaSenha == this.acesso.confirmacaoNovaSenha) {
        if (this.acesso.senha == '') {
          this.notificacao.adicionarMensagem('Para alterar o email ou senha é necessário inserir a senha atual', this.notificacao.BOTTOM).mostrarNotificacoes();
          return; 
        }
      } else {
        this.notificacao.adicionarMensagemErro('A confirmação de nova senha está inválida!', this.notificacao.BOTTOM).mostrarNotificacoes();
        return;
      }
    }

    if (this.acesso.email != this.usuario.get().email) {
      if (this.acesso.senha == '') {
        this.notificacao.adicionarMensagem('Para alterar o email ou senha é necessário inserir a senha atual!', this.notificacao.BOTTOM).mostrarNotificacoes();
        return;
      }
    }

    this.notificacao.mostrarMensagemAguarde('Aguarde a confirmação ...', this.notificacao.BOTTOM);

    if (this.acesso.novaSenha.length > 0 || this.usuario.get().email != this.acesso.email) {

      //Confirmação de senha
      await this.firebase.auth().signInWithEmailAndPassword(this.usuario.get().email, this.acesso.senha).then(async () => {

        let user = this.firebase.auth().currentUser;

        if (this.usuario.get().email != this.acesso.email) {

          //Alteração de email
          await user.updateEmail(this.dados.email).then(() => {
            this.notificacao.adicionarMensagemSucesso('Email alterado com sucesso!', this.notificacao.BOTTOM);
          }).catch((erro) => {
            this.notificacao.adicionarMensagemErro('O email não pode ser alterado!', this.notificacao.BOTTOM);
          });

        }

        if (this.acesso.novaSenha.length > 0) {

            //Alteração de senha
          await user.updatePassword(this.acesso.novaSenha).then(() => {
            this.notificacao.adicionarMensagemSucesso('Senha alterada com sucesso!', this.notificacao.BOTTOM);
          }).catch((erro) => {
            this.notificacao.adicionarMensagemErro('A senha não pode ser alterada!', this.notificacao.BOTTOM);
          });

        }

      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('A senha atual está incorreta! Senha não alterada!', this.notificacao.BOTTOM);
      });

    }

    if (this.foto.capa.length > 0) {
      
      //Envia/Salva foto de capa inserida
      let storageRef = this.firebase.storage().ref('capa/' + this.usuario.get().uid + '.jpg');
      await storageRef.putString(this.foto.capa, 'base64', { contentType: 'image/jpeg' });

      storageRef.getDownloadURL().then((url) => {
        this.dados.fotoCapa = url;
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível alterar imagem da capa!', this.notificacao.BOTTOM);
        this.dados.fotoCapa = this.usuario.get().fotoCapa;
        this.foto.capa;
      });

    }
    
    if (this.foto.perfil.length > 0) {
      //Envia/Salva foto de perfil inserida
      let storageRef = this.firebase.storage().ref('perfil/' + this.usuario.get().uid + '.jpg');
      await storageRef.putString(this.foto.perfil, 'base64', { contentType: 'image/jpeg' }).then(async () => {
        await storageRef.getDownloadURL().then((url) => {
          this.dados.fotoPerfil = url;
        }).catch((erro) => {
          this.notificacao.adicionarMensagemErro('Não foi possível alterar a foto de perfil!', this.notificacao.BOTTOM);
          this.dados.fotPerfil = this.usuario.get().fotoPerfil;
          this.foto.perfil;
        });
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível alterar a foto de perfil!', this.notificacao.BOTTOM);
        this.dados.fotoPerfil = this.usuario.get().fotoPerfil;
        this.foto.perfil;
      });
    }

    //Salva os dados
    await this.firebase.db().collection("usuarios").where('uid', '==', this.usuario.get().uid).get().then((result) => {
      result.docs.forEach(async (doc) => {
        await this.firebase.db().collection("usuarios").doc(doc.id).update({
          ...this.dados
        }).then((result) => {
          this.notificacao.adicionarMensagemSucesso('Dados alterados com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
        }).catch((erro) => {
          this.notificacao.adicionarMensagemErro('Não foi possível alterar os dados!', this.notificacao.BOTTOM).mostrarNotificacoes();
        });
      });
    }).catch((erro) => {
      this.notificacao.adicionarMensagemErro('Não foi possível alterar os dados!', this.notificacao.BOTTOM).mostrarNotificacoes();
    });

  }

}
