import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';
import { dateDataSortValue } from 'ionic-angular/umd/util/datetime-util';
import { checkNoChangesView } from '@angular/core/src/view/view';

/**
 * Generated class for the CriarContaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-conta',
  templateUrl: 'criar-conta.html',
})
export class CriarContaPage {

  public checkConfirm:boolean = false;
  public igrejas = [];
  public igreja;
  public usuario = {
    uid: '',
    email: '',
    senha: '',
    confrimacaoSenha: '',
    nome: '',
    telefone: '',
    fotoPerfil: 'assets/imgs/default-user.jpg',
    fotoCapa: 'assets/imgs/default-banner.jpg',
    bio: '',
    igreja: ''
  };
  public idUsuario;

  constructor(public modal: ModalController, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public firebase: FirebaseProvider, public notificacao: NotificacaoProvider) {
    this.carregarIgrejas();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CriarContaPage');
  }

  async criarConta(){

    await this.firebase.auth().createUserWithEmailAndPassword(this.usuario.email, this.usuario.senha).then(async (user)=>{
        
        //Obtem a url da imagem padrão de capa
        let storageRef = this.firebase.storage().ref('capa/default-banner.jpg');  
        await storageRef.getDownloadURL().then((url) => {
          this.usuario.fotoCapa = url;
        });

        //Obtem a url da imagem padrão de perfil
        storageRef = this.firebase.storage().ref('perfil/default-user.jpg');  
        await storageRef.getDownloadURL().then((url) => {
          this.usuario.fotoPerfil = url;
        });

        //Cadastra dados do usuário registrado
        await this.firebase.db().collection('usuarios').add({

          uid: user.user.uid,
          nome: this.usuario.nome,
          telefone: this.usuario.telefone,
          fotoPerfil: this.usuario.fotoPerfil, 
          fotoCapa: this.usuario.fotoCapa,
          bio: this.usuario.bio,
          email: this.usuario.email,
          igreja: this.igreja,
          administrador: false,
          gerenciador:false

        }).then((data) => {
          this.idUsuario = data.id;
        }).catch((error) => {
          
          this.notificacao.adicionarMensagemErro('Erro ao realizar cadastro!', this.notificacao.BOTTOM).mostrarNotificacoes();
        });
        if(this.checkConfirm){
          await this.firebase.db().collection("solicitacao").add({
            idUser: this.idUsuario,
            nome: this.usuario.nome,
            igreja: this.igreja,
            data: Date.now(),
            analise:false
          }).then(()=> {}).catch((error) =>{
            this.notificacao.adicionarMensagemErro('Erro ao realizar cadastro!', this.notificacao.BOTTOM).mostrarNotificacoes();
          });
        }   
    }).catch((error) => {
      this.notificacao.adicionarMensagemErro('Erro ao realizar cadastro!', this.notificacao.BOTTOM).mostrarNotificacoes();
    });
  }

  login(){
    this.navCtrl.setRoot('LoginPage');
  }

  abrirInformacao(){
    this.modal.create('SobrePage').present();
  }

  async carregarIgrejas(){
    this.firebase.db().collection("igreja").onSnapshot((snapshot)=>{
      this.igrejas = [];
      snapshot.docs.forEach(doc => {
        this.igrejas.push({id: doc.id, ...doc.data()});
      })
    })
  }
  confirm(){
    if(this.checkConfirm){
      let alert = this.alertCtrl.create({
        title: 'Confirme a sua solicitação',
        message: 'A sua solicitação será enviada ao administrador do aplicativo, onde será analisada e podendo ser recusada.\n Deseja prosseguir com a sua solicitação?',
        buttons: [
          {
            text: 'Não',
            role: 'não',
            handler: () => {
              this.checkConfirm = false;
            }
          },
          {
            text: 'Sim',
            role: 'sim'
          }
        ]
      });
      alert.present();
    }
  }
}
