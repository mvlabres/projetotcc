import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';
import { NativeGeocoder, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Http } from '@angular/http';

/**
 * Generated class for the CriarEventoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-criar-evento',
  templateUrl: 'criar-evento.html',
})
export class CriarEventoPage {

  public estados = [];
  public cidades = [];

  public dados = {
    foto: 'assets/imgs/default-banner.jpg',
    nome: '',
    descricao: '',
    data: new Date().toJSON().split('T')[0],
    horario: '',
    nomeContato: '',
    emailContato: '',
    telefoneContato: '',
    informacoesAdicionais: '',
    endereco: {
      estado: '',
      idEstado: '41',
      cidade: '',
      idCidade: '4119905',
      bairro: '',
      rua: '',
      numero: '',
      cep: '',
      referencia: ''
    },
    coordenadas: {
      latitude: '',
      longitude: ''
    }
  };

  public foto = '';

  constructor(public geocoder: NativeGeocoder, public notificacao: NotificacaoProvider, public firebase: FirebaseProvider, public camera: Camera, public navCtrl: NavController, public navParams: NavParams, public view: ViewController, public usuario: UsuarioProvider, public http: Http) {
    this.carregarEstados(false);
    this.carregarCidades(false);
    this.obterDadosCidade();
  }

  alterarImagem(tipo) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 800,
      correctOrientation: true
    }

    if (tipo == 'camera') {
      options.sourceType = this.camera.PictureSourceType.CAMERA;
    } else {
      options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    }

    this.camera.getPicture(options).then((imageData) => {
      this.foto = imageData;
      this.dados.foto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => { });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarEventoPage');
  }

  cancelar() {
    this.dados = {
      foto: 'assets/imgs/default-banner.jpg',
      nome: '',
      descricao: '',
      data: new Date().toJSON().split('T')[0],
      horario: '',
      nomeContato: '',
      emailContato: '',
      telefoneContato: '',
      informacoesAdicionais: '',
      endereco: {
        estado: '',
        idEstado: '41',
        cidade: '',
        idCidade: '4119905',
        bairro: '',
        rua: '',
        numero: '',
        cep: '',
        referencia: ''
      },
      coordenadas: {
        latitude: '',
        longitude: ''
      }
    }
    this.foto = '';
  }

  async obterCoordenadas() {
    //Salva coordenados do endereço cadastro
    let endereco = this.dados.endereco.rua + ' ' + this.dados.endereco.numero + ' ' 
                  + this.dados.endereco.cidade + ' ' + this.dados.endereco.estado + ' ' 
                  + this.dados.endereco.cep;
    await this.geocoder.forwardGeocode(endereco)
      .then((coordinates: NativeGeocoderForwardResult) => {
        this.dados.coordenadas.latitude = coordinates[0].latitude;
        this.dados.coordenadas.longitude = coordinates[0].longitude;
      }).catch((erro) => {
        this.notificacao.adicionarMensagemErro('Não foi possível salvar as coordenadas do evento.'+ 
                                                'O evento não será exibido no mapa!', this.notificacao.BOTTOM);
      });
  }

  async salvar() {

      //Obtem url da imagem padrão de evento
    let storageRef = this.firebase.storage().ref('eventos/default-banner.jpg');
    await storageRef.getDownloadURL().then((url) => {
      this.dados.foto = url;
    });

    this.notificacao.mostrarMensagemAguarde('Aguarde a confirmação ...', this.notificacao.BOTTOM);
    await this.obterCoordenadas();

    //Salva dados do evento
    await this.firebase.db().collection('eventos').add({
      idUsuario: this.usuario.get().uid,
      ...this.dados,
      dataPublicacao: new Date().toJSON().split('T')[0],
      dataAlteracao: new Date().toJSON().split('T')[0]
    }).then(async (doc) => {

      if (this.foto.length > 0) {
        //Envia/Salva a imagem de evento inserida (camera ou arquivo do dispositivo)
        let storageRef = this.firebase.storage().ref('eventos/' + doc.id + '.jpg');
        await storageRef.putString(this.foto, 'base64', { contentType: 'image/jpeg' }).then(async () => {
          await storageRef.getDownloadURL().then(async (url) => {
            this.dados.foto = url;
            await this.firebase.db().collection('eventos').doc(doc.id).update({
              foto: this.dados.foto
            }).then(() => { }).catch((error) => {
              this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', this.notificacao.BOTTOM);
              this.dados.foto = 'assets/imgs/default-banner.jpg';
              this.foto = '';
            });
          }).catch((erro) => {
            this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', this.notificacao.BOTTOM);
            this.dados.foto = 'assets/imgs/default-banner.jpg';
            this.foto = '';
          });
        }).catch((erro) => {
          this.notificacao.adicionarMensagemErro('Não foi possível salvar a imagem do evento!', this.notificacao.BOTTOM);
          this.dados.foto = 'assets/imgs/default-banner.jpg';
          this.foto = '';
        });

      }

      this.notificacao.adicionarMensagemSucesso('Evento criado com sucesso!', this.notificacao.BOTTOM).mostrarNotificacoes();
      this.fechar();
    }).catch((error) => {
      this.notificacao.adicionarMensagemErro('Erro ao criar evento!', this.notificacao.BOTTOM).mostrarNotificacoes();
    });


  }

  fechar() {
    this.view.dismiss();
  }

  //Carrega lista de estados para o campo estado (select)
  carregarEstados(selectDefault) {

    let url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados';

    this.http.get(url).subscribe(data => {
      let result = data.json();
      this.estados = [];

      if(selectDefault){
        this.dados.endereco.cidade = result[0].id;
      }
      
      result.forEach((estado) => {
        this.estados.push({
          id: estado.id,
          nome: estado.nome,
          sigla: estado.sigla
        });
      });    
    });

  }

  //Carrega lista de cidades para o campo cidade (select) de acordo com o estado selecionado
  carregarCidades(selectDefault) {

    let url = `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${this.dados.endereco.idEstado}/municipios`;

    this.http.get(url).subscribe(data => {
      let result = data.json();
      this.cidades = [];

      if(selectDefault){
        this.dados.endereco.cidade = result[0].id;
      }

      result.forEach((cidade) => {
        this.cidades.push({
          id: cidade.id,
          nome: cidade.nome
        });
      });   
    });

  }

  //Obtem informações da cidade selecionada (nome da cidade e sigla do estado)
  obterDadosCidade() {

    let url = `https://servicodados.ibge.gov.br/api/v1/localidades/municipios/${this.dados.endereco.idCidade}`;

    this.http.get(url).subscribe(data => {
      let result = data.json();
      this.dados.endereco.estado = result.microrregiao.mesorregiao.UF.sigla;
      this.dados.endereco.cidade = result.nome;
    });

  }

}
