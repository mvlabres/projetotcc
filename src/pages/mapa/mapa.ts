import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, AlertController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { GoogleMapOptions, GoogleMap, GoogleMaps, GoogleMapsEvent } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';


/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {
  public eventos = [];
  public unsubscribe = () => { };
  public filtro = {
    dataInicial: '',
    dataFinal: '',
    texto: ''
  };
  public map: GoogleMap;
  public coordenadas = { latitude: 0, longitude: 0 };
  
  constructor(public alertCtrl: AlertController, public notificacao: NotificacaoProvider, 
              public geolocation: Geolocation, public events: Events, public navCtrl: NavController, 
              public navParams: NavParams, public firebase: FirebaseProvider, 
              public usuario: UsuarioProvider, public modal: ModalController) {
    
    this.filtro = this.navParams.data;
    this.obterCoordendas();
    events.subscribe('filtrar-mapa', () => {
      this.carregarEventos();
    });

  }

  obterCoordendas() {
    this.geolocation.getCurrentPosition().then((posicao) => {
      this.coordenadas.latitude = posicao.coords.latitude;
      this.coordenadas.longitude = posicao.coords.longitude;
      this.iniciarMapa();
    }).catch((erro) => {
      this.iniciarMapa();
    });

  }

  iniciarMapa() {
    let mapOptions: GoogleMapOptions = {
      mapType: "MAP_TYPE_ROADMAP",
      camera: {
        target: {
          lat: this.coordenadas.latitude,
          lng: this.coordenadas.longitude
        },
        zoom: 10
      }
    };
    this.map = GoogleMaps.create('map_canvas', mapOptions);
    this.carregarEventos();
  }

  carregarEventos() {

    this.unsubscribe();

    let ref = this.firebase.db().collection('eventos')
      .orderBy('data', 'desc');

    if (this.filtro.dataInicial != undefined && this.filtro.dataInicial.length > 0) {
      ref = ref.where('data', '>=', this.filtro.dataInicial);
    }

    if (this.filtro.dataFinal != undefined && this.filtro.dataFinal.length > 0) {
      ref = ref.where('data', '<=', this.filtro.dataFinal);
    }

    this.unsubscribe = ref.onSnapshot((snapshot) => {

      this.eventos.forEach((marker) => {
        if(marker != null && marker != undefined)
          marker.remove();
      });

      this.eventos = [];

      snapshot.docs.forEach(doc => {

        let evento = { id: doc.id, ...doc.data()};
        this.criarMarcador(evento);

      });
    });
  }

  async criarMarcador(evento){

    if (evento.coordenadas.latitude && evento.coordenadas.longitude) {

      let marker = await this.map.addMarkerSync({
        title: evento.nome,
        icon: 'red',
        animation: 'DROP',
        position: {
          lat: parseFloat(evento.coordenadas.latitude),
          lng: parseFloat(evento.coordenadas.longitude)
        }
      });

      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        let eventoModal = this.modal.create("EventoPage", { evento: evento, abrirPerfil: true });
        eventoModal.present();
      });

      this.eventos.push(marker);

    }
    
   }

  abrirDetalhes(evento) {
    this.modal.create("EventoPage", {
      evento: evento,
      abrirPerfil: true
    }).present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapaPage');
  }

}
