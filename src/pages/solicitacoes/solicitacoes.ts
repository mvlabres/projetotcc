import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { isTrueProperty } from 'ionic-angular/umd/util/util';
import { NotificacaoProvider } from '../../providers/notificacao/notificacao';

/**
 * Generated class for the SolicitacoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-solicitacoes',
  templateUrl: 'solicitacoes.html',
})
export class SolicitacoesPage {
  public solicitacoes = [];
  public checkConfirm:boolean = false;
  public msg = "";
  constructor(public firebase:FirebaseProvider, public navCtrl: NavController, 
    public navParams: NavParams, public notificacao:NotificacaoProvider) {
    this.buscarSolicitacoes();
  }

  async buscarSolicitacoes(){
    await this.firebase.db().collection('solicitacao')
                            .orderBy('data', 'asc')
                            .onSnapshot((snapshot)=>{
      this.solicitacoes = [];
      snapshot.docs.forEach(doc => {
        if(doc.data().analise == false){
          this.solicitacoes.push({id: doc.id, ...doc.data()})
        } 
      })
    })
  }

  async confirm(solicitacao, idusuario, acesso){
    await this.firebase.db().collection('usuarios').doc(idusuario).update({
      gerenciador: acesso
    });

    await this.firebase.db().collection("solicitacao").doc(solicitacao.id).update({
      analise: true
    })
    
    if(acesso == true) this.msg = "Perfil gerenciador liberado!";
    else this.msg = "Perfil gerenciador negado!";
    this.notificacao.adicionarMensagemSucesso(this.msg, this.notificacao.BOTTOM).mostrarNotificacoes();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SolicitacoesPage');
  }

}
