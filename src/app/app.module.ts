import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { CriarContaPageModule } from '../pages/criar-conta/criar-conta.module';
import { EditarEventoPageModule } from '../pages/editar-evento/editar-evento.module';
import { EventosPageModule } from '../pages/eventos/eventos.module';
import { EventoPageModule } from '../pages/evento/evento.module';
import { HomePageModule } from '../pages/home/home.module';
import { LoginPageModule } from '../pages/login/login.module';
import { MapaPageModule } from '../pages/mapa/mapa.module';
import { MeusEventosPageModule } from '../pages/meus-eventos/meus-eventos.module';
import { MinhaAgendaPageModule } from '../pages/minha-agenda/minha-agenda.module';
import { PerfilPageModule } from '../pages/perfil/perfil.module';
import { RedefinirSenhaPageModule } from '../pages/redefinir-senha/redefinir-senha.module';
import { IgrejasPageModule } from '../pages/igrejas/igrejas.module';

import { UsuarioProvider } from '../providers/usuario/usuario';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { NotificacaoProvider } from '../providers/notificacao/notificacao';

import { Camera } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';
import { Calendar } from '@ionic-native/calendar';
import { GoogleMaps } from '@ionic-native/google-maps';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import {NgxMaskModule} from 'ngx-mask';



@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    NgxMaskModule,
    BrowserModule,
    HttpModule,
    CriarContaPageModule,
    EditarEventoPageModule,
    EventosPageModule,
    EventoPageModule,
    HomePageModule,
    LoginPageModule,
    MapaPageModule,
    MeusEventosPageModule,
    MinhaAgendaPageModule,
    PerfilPageModule,
    RedefinirSenhaPageModule,
    IgrejasPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioProvider,
    FirebaseProvider,
    NotificacaoProvider,
    Camera,
    SocialSharing,
    CallNumber,
    Calendar,
    GoogleMaps,
    NativeGeocoder,
    Geolocation
  ]
})
export class AppModule {}