import { Component, ViewChild } from '@angular/core';
import { Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavController } from 'ionic-angular';

import { UsuarioProvider } from '../providers/usuario/usuario';
import { FirebaseProvider } from './../providers/firebase/firebase';
import { NotificacaoProvider } from './../providers/notificacao/notificacao';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild('menuNav') menuNav: NavController;

  rootPage:any = 'LoginPage';
  public paginaUser = [];
  public paginaGer = [];
  public paginaAdm = [];
  public solicitacoes = 0;

  constructor(public modal: ModalController, platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, 
              public usuario: UsuarioProvider, public firebase: FirebaseProvider, 
              public notificacao: NotificacaoProvider) {
                this.contarSolicitacoes();
    platform.ready().then(() => {
      
      this.firebase.auth().onAuthStateChanged(user => {
        
        if(!user) {
          this.rootPage = 'LoginPage';
        } else { 
          this.rootPage = 'HomePage';
        }

        this.hideSplashScreen();
  
      });
      
    });

    this.paginaUser = [
      {titulo: 'Home', pagina: 'HomePage', icone: '', view:true},
      {titulo: 'Meus Dados', pagina: 'MeusDadosPage', icone: '', view:true},
      {titulo: 'Meu Perfil', pagina: 'PerfilPage', icone: '', view:true}
    ];
    this.paginaGer = [
      {titulo: 'Meus Eventos', pagina: 'MeusEventosPage', icone: '', view:this.usuario.get().gerenciador},
    ];
    this.paginaAdm = [
      {titulo: 'Igrejas', pagina: 'IgrejasPage', icone: '', view:this.usuario.get().administrador},
      {titulo: 'Relatório', pagina: 'RelatoriosPage', icone: '', view:this.usuario.get().administrador},
      {titulo: 'Solicitações', pagina: 'SolicitacoesPage', icone: '', view:this.usuario.get().administrador},
      {titulo: 'Usuários', pagina: 'UsuariosPage', icone: '', view:this.usuario.get().administrador}
    ];
  }

  hideSplashScreen(){
    this.statusBar.styleDefault();
    
    this.splashScreen.hide();
  }
  async contarSolicitacoes(){
    await this.firebase.db().collection('solicitacao').where('analise', "==", false)
                            .onSnapshot((snapshot)=>{
      snapshot.docs.forEach(doc => {
          this.solicitacoes ++;
      })
    })
  }
  abrirPagina(pagina){
    if(pagina == 'PerfilPage'){
      this.modal.create(pagina ,{
        usuario: this.usuario.get()
      }).present();
    }else if(pagina == 'SobrePage'){
      this.modal.create(pagina).present();
    }else{
      this.menuNav.setRoot(pagina);
    }
  }

  sair(){
    this.firebase.auth().signOut();
  }
}

